// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#ifndef RAYTRACER_MONTECARLOTRACE_H
#define RAYTRACER_MONTECARLOTRACE_H

#include <glm/vec3.hpp>
#include <functional>

struct Scene;

#ifdef BRDF
extern uint32_t maxDepth;
using writeToLightFunc = std::function<void(const glm::vec3&,const glm::vec3&)>;
glm::vec3 bdptScene(const Scene& scene, const glm::vec3& origin,
                    glm::vec3& direction, writeToLightFunc lBuff, uint16_t oS, uint16_t oT,
                    bool onlyLength);
glm::vec3 monteCarloScene(const Scene& scene, glm::vec3 orig, glm::vec3 dir);
#else
glm::vec3 monteCarloScene(const Scene& scene, glm::vec3 orig, glm::vec3 dir);
#endif  // BRDF



#endif  // RAYTRACER_MONTECARLOTRACE_H
