// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#ifndef RAYTRACER_CAMERA_H
#define RAYTRACER_CAMERA_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

// A camera is used to describe the origin of our image rays
//
class Camera {
public:
  Camera(float aspectRatio, double fov);
  glm::vec3 generateDirection(float ndcX, float ndcY) const;
  glm::vec2 toNDC(glm::vec3 dir) const;
  void setCtVTransform(glm::dmat4 ctv);
private:
  float m_ratio;
  float m_halfspan;
  glm::dmat4 m_cameraTransform;
  glm::dmat4 m_inverseTransform;
};

#endif  // RAYTRACER_CAMERA_H
