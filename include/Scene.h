// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#ifndef RAYTRACER_SCENE_H
#define RAYTRACER_SCENE_H

#include <array>
#include <cstdint>
#include <vector>

#include <BVH.h>
#include <Material.h>
#include <BRDF.h>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

/// This struct contains all the information that
/// describe the scene. It is pretty big so don't pass it!
struct Scene {
  // Vertex data.
  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> normals;
  std::vector<glm::vec2> uvs;
  // Indices
  std::vector<uint64_t> indices;
  std::vector<uint64_t> normalIndices;
  std::vector<uint64_t> uvIndices;
  // Per Triangle Data.
  std::vector<uint32_t> triangleMats;
#ifdef BRDF
  std::vector<BRDFData> materials;
#else
  std::vector<Material> materials;
#endif  // BRDF
  // Important Centroids (CAUTION) biased towards lots of triangles
  std::vector<uint64_t> importantThings;

  // the comulated area of the lights.
  std::vector<float> comulativeLightArea;
  // the total surface area of all lights in the scene.
  float totalLightArea;

  // BVH Structure.
  std::vector<BVHNode> nodes;
};

void listImportantThings(Scene& scene);

/// Returnes a direction to one of the important triangles.
/// These can be below the surface normal!
std::tuple<glm::vec3, glm::vec3, float> getRandomImportantDir(
    const Material& mat, const Scene& scene, const glm::vec3& pos,
    const glm::vec3& normal, const glm::vec3& nx, const glm::vec3& nz);

#endif  // RAYTRACER_SCENE_H
