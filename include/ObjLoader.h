// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#ifndef RAYTRACER_OBJLOADER_H
#define RAYTRACER_OBJLOADER_H

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <vector>

struct Scene;

void loadObj(const char* filename, Scene& scene);

#endif  // RAYTRACER_OBJLOADER_H
