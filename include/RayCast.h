// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#ifndef RAYTRACER_RAYCAST_H
#define RAYTRACER_RAYCAST_H

#include <cmath>
#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <vector>

struct IntersectionData {
  uint64_t vertexIndex;
  double t;
  double a;
  double b;
};

#define EPSILON 1e-15


bool rayCast(const glm::vec3& origin, const glm::vec3& dir,
             const glm::vec3* vertices, float* const t);

/*bool rayCast(const glm::vec3& origin, const glm::vec3& dir,
             const std::vector<glm::vec3>& tVert,
             std::vector<uint64_t>::iterator start,
             std::vector<uint64_t>::iterator end, double* const t,
             uint8_t* const bestTri); */

extern uint64_t numTriangleTests;

template <typename IndexIterator>
bool rayCast(const glm::vec3& origin, const glm::vec3& dir,
             const std::vector<glm::vec3>& tVert, IndexIterator start,
             IndexIterator end, IntersectionData* dat) {
  bool intersect = false;
  uint64_t pos = 0;
  while (start != end) {
    ++numTriangleTests;
    ++pos;
    auto v0 = tVert[*start++];
    auto e1 = tVert[*start++] - v0;
    auto e2 = tVert[*start++] - v0;
    auto p = glm::cross(dir, e2);
    auto k = glm::dot(e1, p);
    if (k > -EPSILON && k < EPSILON) continue;
    auto f = 1 / k;
    auto s = origin - v0;
    auto a = f * glm::dot(s, p);
    if (a < 0 || a > 1) continue;
    auto q = glm::cross(s, e1);
    auto b = f * glm::dot(dir, q);
    if (b < 0 || a + b > 1) continue;
    auto newT = f * glm::dot(e2, q);
    if (newT < dat->t) {
      intersect = true;
      dat->t = std::min<double>(newT, dat->t);
      dat->a = a;
      dat->b = b;
      dat->vertexIndex = pos - 1;
    }
  }
  return intersect;
}


#endif  // RAYTRACER_RAYCAST_H
