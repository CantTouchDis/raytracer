// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#ifndef RAYTRACER_MATERIAL_H
#define RAYTRACER_MATERIAL_H

#include <glm/vec3.hpp>
#include <stack>
#include <tuple>

struct Material {
  // diffuse
  glm::vec3 dCol;
  // specular
  glm::vec3 sCol;
  // emmisive
  glm::vec3 eCol;
  // IOR
  float ior;
  // type
  bool isTransparent;
};

std::tuple<float, float, glm::vec3, float, glm::vec3> generateSampleForMaterial(
    const Material& mat, const glm::vec3& incident, const glm::vec3& normal,
    std::stack<float>& ior);

std::tuple<float, float, glm::vec3> generateTransparentSamples(
    const Material& mat, const glm::vec3& incident, const glm::vec3& normal,
    std::stack<float>& ior);

float transparentPDF(float phi, float theta);

std::tuple<float, float, glm::vec3> generateCosWeightedHemSamples();

float cosWeightedPDF(float phi, float theta);

void createCoordinateSystem(const glm::vec3& N, glm::vec3& Nt, glm::vec3& Nb);

glm::vec3 uniformHemisphereSpaceSample(float r1, float r2);

glm::vec3 hemisphereToWorld(const glm::vec3& N, const glm::vec3& Nt,
                            const glm::vec3& Nb, const glm::vec3& hemSample);

std::pair<float, float> generateSampleAngles(const Material& mat,
                                             const glm::vec3& omegaO,
                                             const glm::vec3& normal);

glm::vec3 transformSample(const glm::vec3& normal, const glm::vec3& omegaO,
                          const std::tuple<float, float>& sample);

float evalPDF(const Material& mat, const std::tuple<float, float>& sample);

/// Returnes the ammout of light that will be transported.
glm::vec3 evalMaterial(const Material& mat, const glm::vec3& omegaI,
                       const glm::vec3& omegaO, const glm::vec3& normal);

/// Returns the pdf value for a given sample.
float evalPDF(const Material& mat, const glm::vec3& sample);

/// generates a sample for given input.
glm::vec3 generateSample(const Material& mat, const glm::vec3& omegaO,
                         const glm::vec3& normal);

#endif  // RAYTRACER_MATERIAL_H
