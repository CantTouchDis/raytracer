// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#ifndef RAYTRACER_BVH_H
#define RAYTRACER_BVH_H

#include <cstdint>
#include <glm/vec3.hpp>
#include <vector>

#include <iostream>
#include <iterator>

#include <glm/gtx/string_cast.hpp>

struct IntersectionData;
struct Scene;

struct AABB {
  glm::vec3 min;
  glm::vec3 max;
};

struct BVHNode {
  AABB bbox;
  union {
    struct {
      uint64_t leftChild;
      uint64_t rightChild;
    };
    struct {
      uint64_t startIndex;
      uint64_t endIndex;
    };
  };
  bool isLeaf;
};

extern uint32_t lastDepth;
extern uint64_t numAABBTests;
extern uint64_t numRaysCast;

/// Creates a BVH of a triangle list given by the vertices and the indices.
/// The indices might be reordered in order to make the nodes smaller.
void topDownBVH(Scene& scene);

/// Used to traverse a BVH with a ray.
bool traverseBVH(const glm::vec3& origin, const glm::vec3& dir,
                 const std::vector<glm::vec3>& tVerts,
                 const std::vector<uint64_t>& indices,
                 const std::vector<BVHNode>& nodes, size_t currentNodeId,
                 IntersectionData* dat);

void countTraversedBoxes(const glm::vec3& origin, const glm::vec3& dir,
                         const std::vector<BVHNode>& nodes, size_t currentNodeId,
                         uint16_t* numTraversed);


extern glm::vec3 backGround;

glm::vec3 traceThroughScene(const Scene& scene, glm::vec3 orig, glm::vec3 dir);

#endif  // RAYTRACER_BVH_H
