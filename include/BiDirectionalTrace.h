// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#ifndef RAYTRACER_BIDIRECTIONALTRACE_H
#define RAYTRACER_BIDIRECTIONALTRACE_H

#include <functional>
#include <glm/glm.hpp>

namespace bdpt {

using writeToLightFunc = std::function<void(const glm::vec3&,const glm::vec3&)>;

glm::vec3 bdptScene(const Scene& scene, const glm::vec3& origin,
                    glm::vec3& direction, writeToLightFunc lBuffer, uint16_t oS,
                    uint16_t oT, bool onlyLength);
}
#endif  // RAYTRACER_BIDIRECTIONALTRACE_H
