// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#ifndef RAYTRACER_BRDF_H
#define RAYTRACER_BRDF_H

#include <glm/vec3.hpp>
#include <tuple>
#include <stack>

struct BRDFData {
  using sampleInfo = std::tuple<glm::vec3, glm::vec3, float>;
  // diffuse
  glm::vec3 dCol;
  // specular
  glm::vec3 sCol;
  // emmisive
  glm::vec3 eCol;
  // absorption in m^(-1)
  glm::vec3 absorption;
  // IOR
  float ior;
  // n1 and n2 n1 is on the side of the normal.
  float n1, n2;
  //
  /// if this function is set shadowrays may be cast and weighted with it
  glm::vec3 (*evalBRDF)(const BRDFData& mat, const glm::vec3& normal, const glm::vec3& in, const glm::vec3& out);
  /// this function is used to generate samples with the respective brdf value and pdf.
  std::tuple<glm::vec3, glm::vec3, float> (*generateSampleFunc)(
      const BRDFData& mat, const glm::vec3& incident, const glm::vec3& normal,
      int32_t& currentMat, int32_t& nextMat);
};

glm::vec3 lambertTransmission(const BRDFData& mat, float distanceInMaterial);

std::tuple<glm::vec3, glm::vec3, float> bsdfSampler(
    const BRDFData& mat, const glm::vec3& incident, const glm::vec3& normal,
    int32_t& currentMat, int32_t& nextMat);

glm::vec3 diffuse(const BRDFData& mat, const glm::vec3& normal, const glm::vec3& in, const glm::vec3& out);

std::tuple<glm::vec3, glm::vec3, float> cosineWeightedSampler(
    const BRDFData& mat, const glm::vec3& incident, const glm::vec3& normal,
    int32_t& currentMat, int32_t& nextMat);

std::tuple<glm::vec3, glm::vec3, float> uniformSampler(
    const BRDFData& mat, const glm::vec3& incident, const glm::vec3& normal,
    int32_t& currentMat, int32_t& nextMat);

#endif  // RAYTRACER_BRDF_H
