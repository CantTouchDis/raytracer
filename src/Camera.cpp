// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#include <Camera.h>
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>

Camera::Camera(float aspectRatio, double fov) : m_ratio(aspectRatio) {
  m_halfspan = tan(fov / 2);
}

glm::vec3 Camera::generateDirection(float ndcX, float ndcY) const {
  glm::vec3 dir((2 * ndcX - 1) * m_halfspan * m_ratio,
                (1 - 2 * ndcY) * m_halfspan, -1);
  // std::cout << "NDC before transform (" << ndcX << "|" << ndcY << ")" << std::endl;
  // std::cout << "Vector before transform " << glm::to_string(dir) << std::endl;
  dir = glm::vec3(m_cameraTransform * glm::vec4(dir, 0.0));
  dir = glm::normalize(dir);
  return dir;
}

glm::vec2 Camera::toNDC(glm::vec3 dir) const {
  // dir.x = (2 * ndcX - 1) * m_half * m_ratio
  // dir.x / (m_half * m_ratio)
  auto val = m_inverseTransform * glm::vec4(dir, 0.0f);
  val /= -val.z;
  // std::cout << "Vector after transform " << glm::to_string(val) << std::endl;
  auto ndcX = val.x / (2.0f * m_halfspan * m_ratio) + 0.5f;
  auto ndcY = val.y / (-2.0f * m_halfspan) + 0.5f;
  // std::cout << "NDC before transform (" << ndcX << "|" << ndcY << ")" << std::endl;
  return glm::vec2(ndcX, ndcY);
}

void Camera::setCtVTransform(glm::dmat4 ctv) {
  m_cameraTransform = ctv;
  m_inverseTransform = glm::inverse(m_cameraTransform);
}
