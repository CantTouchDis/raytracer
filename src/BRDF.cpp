// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#include <BRDF.h>
#include <random>
#include <stack>
#include <tuple>
#include <vector>

#include <glm/glm.hpp>

namespace {
std::random_device rd;
std::mt19937 generator(rd());
std::uniform_real_distribution<float> distribution(0, 1);
std::uniform_real_distribution<float> whichDir(0, 1);

void createCoordinateSystem(const glm::vec3& N, glm::vec3& Nt, glm::vec3& Nb) {
  if (std::fabs(N.x) > std::fabs(N.y))
    Nt = glm::vec3(N.z, 0, -N.x) / sqrtf(N.x * N.x + N.z * N.z);
  else
    Nt = glm::vec3(0, -N.z, N.y) / sqrtf(N.y * N.y + N.z * N.z);
  Nb = glm::cross(N, Nt);
}

glm::vec3 hemisphereToWorld(const glm::vec3& N, const glm::vec3& Nt,
                            const glm::vec3& Nb, const glm::vec3& hemSample) {
  return glm::vec3(hemSample.x * Nb.x + hemSample.y * N.x + hemSample.z * Nt.x,
                   hemSample.x * Nb.y + hemSample.y * N.y + hemSample.z * Nt.y,
                   hemSample.x * Nb.z + hemSample.y * N.z + hemSample.z * Nt.z);
}
}  // namespace

glm::vec3 lambertTransmission(const BRDFData& mat, float distanceInMaterial) {
  auto tmp = -distanceInMaterial * mat.absorption;
  return glm::vec3(expf(tmp.r), expf(tmp.g), expf(tmp.b));
}

glm::vec3 diffuse(const BRDFData& mat, const glm::vec3& normal,
                  const glm::vec3& in, const glm::vec3& out) {
  return mat.dCol / static_cast<float>(M_PI);
}

std::tuple<glm::vec3, glm::vec3, float> cosineWeightedSampler(
    const BRDFData& mat, const glm::vec3& incident, const glm::vec3& normal,
    int32_t& currentMat, int32_t& nextMat) {
  // Keep the current traversed material
  nextMat = currentMat;
  glm::vec3 nx;
  glm::vec3 nz;
  createCoordinateSystem(normal, nx, nz);
  auto r1 = distribution(generator);
  auto r2 = distribution(generator);
  auto theta = -asinf(sqrtf(2 * r1 / M_PI));
  auto phi = 2 * M_PI * r2;
  auto sinTheta = sinf(theta);
  auto cosTheta = cosf(theta);
  glm::vec3 localSample(sinTheta * cosf(phi), cosTheta, sinTheta * sinf(phi));
  auto worldSample = hemisphereToWorld(normal, nx, nz, localSample);
  auto pdfValue = (1.0f) / (M_PI);
  return std::make_tuple(
      worldSample, mat.evalBRDF(mat, normal, incident, worldSample), pdfValue);
}

std::tuple<glm::vec3, glm::vec3, float> uniformSampler(
    const BRDFData& mat, const glm::vec3& incident, const glm::vec3& normal,
    int32_t& currentMat, int32_t& nextMat) {
  // Keep the current traversed material
  nextMat = currentMat;
  glm::vec3 nx;
  glm::vec3 nz;
  createCoordinateSystem(normal, nx, nz);
  auto r1 = distribution(generator);
  auto r2 = distribution(generator);
  auto theta = acosf(1.0f - r1);
  auto phi = 2 * M_PI * r2;
  auto sinTheta = sinf(theta);
  auto cosTheta = cosf(theta);
  glm::vec3 localSample(sinTheta * cosf(phi), cosTheta, sinTheta * sinf(phi));
  auto worldSample = hemisphereToWorld(normal, nx, nz, localSample);
  auto pdfValue = 1.0f / (2 * M_PI);
  return std::make_tuple(
      worldSample, mat.evalBRDF(mat, normal, incident, worldSample), pdfValue);
}

std::tuple<glm::vec3, glm::vec3, float> bsdfSampler(const BRDFData& mat,
                                                    const glm::vec3& incident,
                                                    const glm::vec3& normal,
                                                    int32_t& currentMat,
                                                    int32_t& nextMat) {
  // Calculate fresnel
  auto cosThetaIn = glm::dot(normal, incident);
  auto referenceNormal = normal;
  auto n1 = 1.0f;
  auto n2 = 1.0f;
  bool switched = false;
  // Leaving the material?
  if (cosThetaIn < 0) {
    switched = true;
    // Make sure the currentMaterial is correct. (could be wrong if we start
    // inside)
    currentMat = nextMat;
    // the indices have to be inverted
    n1 = mat.n2;
    n2 = mat.n1;
    // Turn the reference normal around and adjust the angle.
    referenceNormal = -normal;
    cosThetaIn = glm::dot(referenceNormal, incident);
  } else {
    // the surface belongs to the medium
    n1 = mat.n1;
    n2 = mat.n2;
  }
  // compute snell
  auto critical = asinf(n2 / n1);
  // TODO(bauschp): there is a way around this mess.
  auto angleR = asinf(sinf(acosf(cosThetaIn)) * (n1 / n2));
  auto cosThetaRefracted = cosf(angleR);
  // compute fresnel
  auto reflectanceS = (n1 * cosThetaIn - n2 * cosThetaRefracted) /
                      (n1 * cosThetaIn + n2 * cosThetaRefracted);
  reflectanceS *= reflectanceS;
  auto reflectanceP = (n1 * cosThetaRefracted - n2 * cosThetaIn) /
                      (n1 * cosThetaRefracted + n2 * cosThetaIn);
  reflectanceP *= reflectanceP;
  auto unpolRefl = (reflectanceP + reflectanceS) * 0.5f;
  // do we have total internal reflection?
  if (acosf(cosThetaIn) > critical) unpolRefl = 1.0f;
  if (whichDir(generator) < unpolRefl) {
    // Return a reflected sample.
    if (!switched) {
      nextMat = currentMat;
    }
    auto reflectedDir = 2 * glm::dot(normal, incident) * normal - incident;
    // TODO(bauschp): this is to get the negative term out of the integral.
    auto d = glm::dot(reflectedDir, normal);
    auto invDot = fabs(d) < 1e-4 ? 0.0f : 1.0f / d;
    return std::make_tuple(reflectedDir, glm::vec3(1.0f) * unpolRefl,
                           unpolRefl * fabs(invDot));
  } else {
    // Return a refracted sample.
    if (switched) {
      // We don't know what's next. Lets see.
      nextMat = -1;
    }
    auto ratio = n1 / n2;
    // Heckbert
    auto refractedDir = (ratio * cosThetaIn - cosf(angleR)) * referenceNormal -
                        ratio * incident;
    auto d = glm::dot(refractedDir, normal);
    // avoid division by zero.
    auto invDot = fabs(d) < 1e-4 ? 0.0f : 1.0f / d;
    return std::make_tuple(refractedDir,
                           glm::vec3(1.0f) * (1 - unpolRefl),
                           (1 - unpolRefl) * fabs(invDot));
  }
}
