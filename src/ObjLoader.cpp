// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#include <ObjLoader.h>
#include <Scene.h>
#include <inttypes.h>
#include <algorithm>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <set>

void readMaterials(const char* mtllibName,
                   const std::vector<std::string>& materialNames,
                   Scene& scene) {
  FILE* file = fopen(mtllibName, "r");
  if (file == nullptr) {
    std::cerr << "Couldn't open Material File: " << mtllibName << std::endl;
    return;
  }
  uint8_t currentMat = 0;
  while (true) {
    char lineHeader[128];
    int res = fscanf(file, "%s", lineHeader);
    if (res == EOF) break;  // EOF = End Of File. Quit the loop.
    if (strcmp(lineHeader, "newmtl") == 0) {
      char buffer[255];
      if (fscanf(file, "%s\n", &buffer[0]) != 1) {
        std::cerr << "Error reading Material Name." << std::endl;
      } else {
        auto elem = std::find(materialNames.begin(), materialNames.end(),
                              std::string(buffer));
        if (elem == materialNames.end()) {
          std::cerr << "Material was not used." << std::endl;
          continue;
        }
        // Remember position
        currentMat = std::distance(materialNames.begin(), elem);
      }
    } else if (strcmp(lineHeader, "Kd") == 0) {
      float r, g, b;
      if (fscanf(file, "%f %f %f\n", &r, &g, &b) != 3) {
        std::cerr << "Error reading Material Name." << std::endl;
      } else {
        scene.materials[currentMat].dCol = glm::vec3(r, g, b);
      }
    } else if (strcmp(lineHeader, "Ks") == 0) {
      float r, g, b;
      if (fscanf(file, "%f %f %f\n", &r, &g, &b) != 3) {
        std::cerr << "Error reading Material Name." << std::endl;
      } else {
        scene.materials[currentMat].sCol = glm::vec3(r, g, b);
      }
    } else if (strcmp(lineHeader, "Ke") == 0) {
      float r, g, b;
      if (fscanf(file, "%f %f %f\n", &r, &g, &b) != 3) {
        std::cerr << "Error reading Material Name." << std::endl;
      } else {
        scene.materials[currentMat].eCol = glm::vec3(r, g, b);
      }
    } else if (strcmp(lineHeader, "illum") == 0) {
      int i;
      if (fscanf(file, "%d\n", &i) != 1) {
        std::cerr << "Error reading (illum) value." << std::endl;
      } else {
        // scene.materials[currentMat].isTransparent = i == 6;
        if (i == 6 || i == 9) {
#ifdef BRDF
          scene.materials[currentMat].generateSampleFunc = bsdfSampler;
          scene.materials[currentMat].evalBRDF = nullptr;
#else
          scene.materials[currentMat].isTransparent = i == 6;
#endif  // BRDF
        } else {
#ifdef BRDF
          scene.materials[currentMat].generateSampleFunc =
              cosineWeightedSampler;
          scene.materials[currentMat].evalBRDF = diffuse;
#endif  // BRDF
        }
      }
    } else if (strcmp(lineHeader, "Ni") == 0) {
      float ior;
      if (fscanf(file, "%f\n", &ior) != 1) {
        std::cerr << "Error reading (Ni) value." << std::endl;
      } else {
        // Fallback values.
        scene.materials[currentMat].n1 = 1.0f;
        scene.materials[currentMat].n2 = ior;
      }
    } else if (strcmp(lineHeader, "absorb") == 0) {
      glm::vec3 absorb;
      if (fscanf(file, "%f %f %f\n", &absorb.r, &absorb.g, &absorb.b) != 3) {
        std::cerr << "Error reading (absorb) value." << std::endl;
      } else {
        // Fallback values.
        scene.materials[currentMat].absorption = absorb;
      }
    } else if (strcmp(lineHeader, "refr") == 0) {
      float n1;
      float n2;
      if (fscanf(file, "%f %f\n", &n1, &n2) != 2) {
        std::cerr << "Error reading (refr) value." << std::endl;
      } else {
        // Fallback values.
        scene.materials[currentMat].n1 = n1;
        scene.materials[currentMat].n2 = n2;
      }
    } else {
      std::cerr << "Unknown line: " << lineHeader << std::endl;
    }
  }
  fclose(file);
}

void loadObj(const char* filename, Scene& scene) {
  // loadObj(filename, scene.vertices, scene.normals, scene.indices,
  //        scene.normalIndices, scene.triangleMats);
  FILE* file = fopen(filename, "r");
  if (file == nullptr) {
    std::cerr << "Can't open file!" << std::endl;
    return;
  }
  scene.vertices.clear();
  scene.normals.clear();
  scene.indices.clear();
  scene.triangleMats.clear();
  scene.normalIndices.clear();
  std::set<std::string> materialLibs;
  std::vector<std::string> matOrder;
  uint32_t currentMat = 0;
  while (true) {
    char lineHeader[128];
    // read the first word of the line
    // TODO(bauschp): safe?
    int res = fscanf(file, "%s", lineHeader);
    if (res == EOF) break;  // EOF = End Of File. Quit the loop.

    // lineHeader contains now one of the following.
    if (strcmp(lineHeader, "v") == 0) {
      // A Vertex
      glm::vec3 vert;
      fscanf(file, "%f %f %f\n", &vert.x, &vert.y, &vert.z);
      scene.vertices.push_back(vert);
    } else if (strcmp(lineHeader, "vt") == 0) {
      // Texture Vertex (uv coord)
    } else if (strcmp(lineHeader, "vn") == 0) {
      glm::vec3 normal;
      fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
      scene.normals.push_back(normal);
      // Vertex Normal
    } else if (strcmp(lineHeader, "f") == 0) {
      // A Face
      scene.triangleMats.push_back(currentMat);
      uint64_t vertexIndex[3], uvIndex[3], normalIndex[3];
      char line[256];
      if (fgets(line, sizeof(line), file) == NULL) break;
      int matches = sscanf(line, "%" SCNu64 " %" SCNu64 " %" SCNu64 "\n",
                           &vertexIndex[0], &vertexIndex[1], &vertexIndex[2]);
      if (matches == 3) {
        scene.indices.push_back(vertexIndex[0] - 1);
        scene.indices.push_back(vertexIndex[1] - 1);
        scene.indices.push_back(vertexIndex[2] - 1);
        continue;
      }
      matches = sscanf(line, "%" SCNu64 "//%" SCNu64 "%" SCNu64 "//%" SCNu64
                             " %" SCNu64 "//%" SCNu64 "\n",
                       &vertexIndex[0], &normalIndex[0], &vertexIndex[1],
                       &normalIndex[1], &vertexIndex[2], &normalIndex[2]);
      if (matches == 6) {
        scene.indices.push_back(vertexIndex[0] - 1);
        scene.indices.push_back(vertexIndex[1] - 1);
        scene.indices.push_back(vertexIndex[2] - 1);
        scene.normalIndices.push_back(normalIndex[0] - 1);
        scene.normalIndices.push_back(normalIndex[1] - 1);
        scene.normalIndices.push_back(normalIndex[2] - 1);
        continue;
      }

      matches =
          sscanf(line, "%" SCNu64 "/%" SCNu64 "/%" SCNu64 "%" SCNu64 "/%" SCNu64
                       "/%" SCNu64 " %" SCNu64 "/%" SCNu64 "/%" SCNu64 "\n",
                 &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1],
                 &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2],
                 &normalIndex[2]);
      if (matches == 9) {
        scene.indices.push_back(vertexIndex[0] - 1);
        scene.indices.push_back(vertexIndex[1] - 1);
        scene.indices.push_back(vertexIndex[2] - 1);
        scene.normalIndices.push_back(normalIndex[0] - 1);
        scene.normalIndices.push_back(normalIndex[1] - 1);
        scene.normalIndices.push_back(normalIndex[2] - 1);
        continue;
      }
    } else if (strcmp(lineHeader, "mtllib") == 0) {
      char buff[100];
      fscanf(file, "%s\n", &buff[0]);
      materialLibs.emplace(buff);
    } else if (strcmp(lineHeader, "usemtl") == 0) {
      char buff[100];
      fscanf(file, "%s\n", &buff[0]);
      auto pos = std::find(matOrder.begin(), matOrder.end(), std::string(buff));
      if (pos == matOrder.end()) {
        currentMat = matOrder.size();
        matOrder.emplace_back(buff);
      } else {
        currentMat = std::distance(matOrder.begin(), pos);
      }
    }
  }
  std::cout << "Done reading file." << std::endl;
  std::cout << "Found " << scene.vertices.size() << " Vertices and "
            << scene.indices.size() << " Indices." << std::endl;
  std::cout << "Found " << scene.normals.size() << " Normals and "
            << scene.indices.size() << " NormalIndices." << std::endl;
  std::cout << "Found the following materials ";
  std::for_each(matOrder.begin(), matOrder.end(),
                [](const auto& n) { std::cout << n << ", "; });
  std::cout << std::endl;
  // Don't forget to close the file.
  fclose(file);
  std::string path(filename);
  path = path.substr(0, path.find_last_of("\\/"));
  scene.materials.clear();
  scene.materials.resize(matOrder.size());
  for (auto it = materialLibs.begin(); it != materialLibs.end(); ++it) {
    std::string mtlLib = path + "/" + *it;
    readMaterials(mtlLib.c_str(), matOrder, scene);
  }
  // Now lets read the materials
}
