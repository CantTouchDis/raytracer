// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#include <BVH.h>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <stack>

#include <Material.h>
#include <RayCast.h>
#include <Scene.h>
#include <glm/gtx/string_cast.hpp>
#include <glm/vec3.hpp>

auto costF = [](size_t numT, const glm::vec3& dim) {
  return numT * 2 * (dim.x * dim.y + dim.x * dim.z + dim.y * dim.z);
};

/// Reorders the elements so they will be at the position given by the order
/// iterators. By @Potatoswatter (StackOverflow)
template <typename order_iterator, typename value_iterator>
void reorder(order_iterator order_begin, order_iterator order_end,
             value_iterator v, char size = 1) {
  using namespace std;
  typedef typename iterator_traits<value_iterator>::value_type value_t;
  typedef typename iterator_traits<order_iterator>::value_type index_t;
  typedef typename iterator_traits<order_iterator>::difference_type diff_t;

  diff_t remaining = order_end - 1 - order_begin;
  for (index_t s = index_t(), d; remaining > 0; ++s) {
    for (d = order_begin[s]; d > s; d = order_begin[d])
      ;
    if (d == s) {
      --remaining;
      std::vector<value_t> tmps(size);
      for (auto i = 0; i < size; ++i) tmps[i] = v[s * size + i];
      // value_t temp = v[s];
      while (d = order_begin[d], d != s) {
        for (auto i = 0; i < size; ++i) swap(tmps[i], v[d * size + i]);
        // swap(temp, v[d]);
        --remaining;
      }
      for (auto i = 0; i < size; ++i) v[s * size + i] = tmps[i];
      // v[s] = temp;
    }
  }
}

uint32_t lastDepth = 0;
glm::vec3 backGround(0.2f);

void recurseTopDownBVH(const std::vector<glm::vec3>& tVerts,
                       std::vector<glm::vec3>& centroids,
                       std::vector<uint64_t>& indices, size_t start, size_t end,
                       std::vector<BVHNode>& nodes, size_t currentNodeId,
                       uint8_t depth, std::vector<uint64_t>& realOrder) {
  lastDepth = std::max<uint32_t>(lastDepth, depth);
  BVHNode& currentNode = nodes[currentNodeId];
  auto numTri = (end - start) / 3;
  if (numTri < 4) {
    currentNode.isLeaf = true;
    currentNode.startIndex = start;
    currentNode.endIndex = end;
    return;
  }
  // Prob not a leaf!
  currentNode.isLeaf = false;
  // Calculate the current cost!
  auto bboxDim = currentNode.bbox.max - currentNode.bbox.min;
  auto currentCost = costF(numTri, bboxDim);
  // How many splits should we check?
  uint16_t nBins = depth < 10 ? (1024 >> depth) : 2;
  // Keep track of the best split
  auto bestCost = currentCost;
  char bestAxis = -1;
  size_t bestSplit;
  AABB bestLeft;
  AABB bestRight;
  // Loop over all axis.
  for (char axis = 0; axis < 3; ++axis) {
    // compute the distance between split checks.
    double binDist = bboxDim[axis] / nBins;
    // try out all the splits.
    for (size_t i = 1; i < nBins; ++i) {
      AABB newLeft = {glm::vec3(std::numeric_limits<double>::max()),
                      glm::vec3(std::numeric_limits<double>::lowest())};
      AABB newRight = newLeft;
      uint64_t numLeft = 0;
      uint64_t numRight = 0;
      // compute where the split is at.
      float splitAt = currentNode.bbox.min[axis] + binDist * i;
      auto ix = indices.begin() + start;
      // Expand the bboxes according to the spliting pane.
      auto expandAABB = [&](AABB& aabb, uint64_t& num) {
        ++num;
        for (char i = 0; i < 3; ++i) {
          aabb.min = glm::min(aabb.min, tVerts[*ix]);
          aabb.max = glm::max(aabb.max, tVerts[*ix]);
          ++ix;
        }
      };
      for (auto cent = centroids.begin() + start / 3;
           cent != centroids.begin() + start / 3 + numTri; ++cent) {
        if (cent->operator[](axis) < splitAt) {
          expandAABB(newLeft, numLeft);
        } else {
          expandAABB(newRight, numRight);
        }
      }
      if (ix == indices.begin() + end) {
        // std::cout << "ok" << std::endl;
      } else {
        std::cout << std::distance(ix, indices.begin() + end) << std::endl;
      }
      // ignore dumb splits!
      if (numLeft < 2 || numRight < 2) continue;
      // compute the costs.
      auto leftDim = newLeft.max - newLeft.min;
      auto leftCost = costF(numLeft, leftDim);
      auto rightDim = newRight.max - newRight.min;
      auto rightCost = costF(numRight, rightDim);
      // if the costs are lower than the current minimum remember it.
      if (bestCost > leftCost + rightCost) {
        bestCost = leftCost + rightCost;
        bestAxis = axis;
        bestSplit = i;
        bestLeft = newLeft;
        bestRight = newRight;
      }
    }
  }
  // if there is no good split
  if (bestAxis == -1) {
    currentNode.isLeaf = true;
    currentNode.startIndex = start;
    currentNode.endIndex = end;
    return;
  }
  // Now there comes the real split
  float splitAt =
      currentNode.bbox.min[bestAxis] + (bboxDim[bestAxis] / nBins) * bestSplit;
  std::vector<size_t> order(numTri);
  std::iota(order.begin(), order.end(), static_cast<size_t>(0));
  auto mid =
      std::partition(order.begin(), order.end(),
                     [&centroids, bestAxis, splitAt, start](const auto& index) {
                       return centroids[start / 3 + index][bestAxis] < splitAt;
                     });
  reorder(order.begin(), order.end(), centroids.begin() + start / 3);
  reorder(order.begin(), order.end(), indices.begin() + start, 3);
  reorder(order.begin(), order.end(), realOrder.begin() + start / 3);
  auto midPos = start + std::distance(order.begin(), mid) * 3;
  // Left tree.
  auto leftChildId = nodes.size();
  nodes.emplace_back();
  nodes.back().bbox = bestLeft;
  // Right tree.
  auto rightChildId = nodes.size();
  nodes.emplace_back();
  nodes.back().bbox = bestRight;
  // emplacing elements has caused our reference to dangle!
  nodes[currentNodeId].leftChild = leftChildId;
  nodes[currentNodeId].rightChild = rightChildId;

  // ERROR
  /*for (uint64_t id = midPos; id < end; ++id) {
    const auto& v = tVerts[indices[id]];
    const auto& b = bestRight;
    const auto& c = centroids[id / 3];
    std::vector<bool> wut = {v.x > b.max.x, v.y > b.max.y, v.z > b.max.z,
                             v.x < b.min.x, v.y < b.min.y, v.z < b.min.z};
    bool any = false;
    std::for_each(wut.begin(), wut.end(),
                  [&any](const auto& v) { any = any | v; });
    if (any) {
      std::cout << "BBOX(" << glm::to_string(b.min) << "|"
                << glm::to_string(b.max) << ")" << std::endl;
      std::cout << "Triangle Cent is " << glm::to_string(c) << std::endl;
      std::cout << "Vertex is " << glm::to_string(v) << std::endl;
      std::cout << "THAT SHOULD NOT HAPPEN EVER" << std::endl;
    }
  }*/

  recurseTopDownBVH(tVerts, centroids, indices, start, midPos, nodes,
                    leftChildId, depth + 1, realOrder);

  recurseTopDownBVH(tVerts, centroids, indices, midPos, end, nodes,
                    rightChildId, depth + 1, realOrder);
}

void topDownBVH(Scene& scene) {
  // topDownBVH(scene.vertices, scene.indices, scene.normalIndices,
  // scene.uvIndices, scene.nodes);
  // compute all centroids.
  std::vector<glm::vec3> centroids(scene.indices.size() / 3);
  // Remember at what position what centroid is.
  auto index = scene.indices.begin();
  for (size_t i = 0, size = centroids.size(); i < size; ++i) {
    centroids[i] = scene.vertices[*index++] + scene.vertices[*index++] +
                   scene.vertices[*index++];
    centroids[i] /= 3;
  }
  // extract into own function
  BVHNode node;
  // Build AABB
  node.bbox = {scene.vertices[0], scene.vertices[0]};
  // loop over list and expand bbox of node
  for (index = scene.indices.begin(); index != scene.indices.end(); ++index) {
    node.bbox.min = glm::min(node.bbox.min, scene.vertices[*index]);
    node.bbox.max = glm::max(node.bbox.max, scene.vertices[*index]);
  }
  std::cout << "BBox(" << glm::to_string(node.bbox.min) << ","
            << glm::to_string(node.bbox.max) << std::endl;
  scene.nodes.clear();
  scene.nodes.push_back(node);
  std::vector<uint64_t> order(centroids.size());
  std::iota(begin(order), end(order), static_cast<size_t>(0));

  recurseTopDownBVH(scene.vertices, centroids, scene.indices, 0,
                    scene.indices.size(), scene.nodes, 0, 0, order);

  std::vector<uint64_t> revOrder(centroids.size());
  std::iota(begin(revOrder), end(revOrder), static_cast<size_t>(0));

  // TODO(bauschp): messed up the order array. So resort here.
  std::sort(
      revOrder.begin(), revOrder.end(),
      [&order](const auto& a, const auto& b) { return order[a] < order[b]; });

  if (scene.indices.size() == scene.normalIndices.size())
    reorder(revOrder.begin(), revOrder.end(), scene.normalIndices.begin(), 3);
  if (scene.indices.size() == scene.uvIndices.size())
    reorder(revOrder.begin(), revOrder.end(), scene.uvIndices.begin(), 3);
  if (scene.indices.size() == scene.triangleMats.size() * 3) {
    std::cout << "Reordering materials." << std::endl;
    reorder(revOrder.begin(), revOrder.end(), scene.triangleMats.begin());
  }
}

uint64_t numAABBTests = 0;

bool aabbHit(const AABB& b, const glm::vec3& origin, const glm::vec3& dir,
             double& tout) {
  ++numAABBTests;
  auto invDir = glm::vec3(1.0, 1.0, 1.0) / dir;

  // TODO(bauschp): think about a vector aproach.
  auto tx1 = (b.min.x - origin.x) * invDir.x;
  auto tx2 = (b.max.x - origin.x) * invDir.x;

  auto tmin = std::min(tx1, tx2);
  auto tmax = std::max(tx1, tx2);

  auto ty1 = (b.min.y - origin.y) * invDir.y;
  auto ty2 = (b.max.y - origin.y) * invDir.y;

  tmin = std::max(tmin, std::min(ty1, ty2));
  tmax = std::min(tmax, std::max(ty1, ty2));

  auto tz1 = (b.min.z - origin.z) * invDir.z;
  auto tz2 = (b.max.z - origin.z) * invDir.z;

  tmin = std::max(tmin, std::min(tz1, tz2));
  tmax = std::min(tmax, std::max(tz1, tz2));
  tout = tmin;
  return tmax >= tmin;
}

uint64_t numRaysCast = 0;

void countTraversedBoxes(const glm::vec3& origin, const glm::vec3& dir,
                         const std::vector<BVHNode>& nodes, size_t currentNodeId,
                         uint16_t* numTraversed) {
  *numTraversed = 0;
  std::stack<size_t> openNodes;
  openNodes.push(currentNodeId);
  while (!openNodes.empty()) {
    // get the next node.
    auto current = openNodes.top();
    openNodes.pop();
    double tbox = -1;
    if (!aabbHit(nodes[current].bbox, origin, dir, tbox)) continue;
    *numTraversed += 1;
    if (!nodes[current].isLeaf) {
      openNodes.push(nodes[current].leftChild);
      openNodes.push(nodes[current].rightChild);
      continue;
    }
  }
}

bool traverseBVH(const glm::vec3& origin, const glm::vec3& dir,
                 const std::vector<glm::vec3>& tVerts,
                 const std::vector<uint64_t>& indices,
                 const std::vector<BVHNode>& nodes, size_t currentNodeId,
                 IntersectionData* dat) {
  ++numRaysCast;
  // keep track of the smallest intersection so far.
  IntersectionData bestIntersection;
  bestIntersection.t = std::numeric_limits<double>::max();
  std::stack<size_t> openNodes;
  openNodes.push(currentNodeId);
  bool intersected = false;
  while (!openNodes.empty()) {
    // get the next node.
    auto current = openNodes.top();
    openNodes.pop();
    double tbox = -1;
    if (!aabbHit(nodes[current].bbox, origin, dir, tbox)) continue;
    // We hit the node. Lets check if its behind the best accual hit so far.
    if (tbox > bestIntersection.t) continue;
    if (!nodes[current].isLeaf) {
      openNodes.push(nodes[current].leftChild);
      openNodes.push(nodes[current].rightChild);
      continue;
    }
    // This is a leaf. Lets check the triangles for intersections.
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    if (rayCast(origin, dir, tVerts,
                indices.begin() + nodes[current].startIndex,
                indices.begin() + nodes[current].endIndex, &data) &&
        bestIntersection.t > data.t && data.t > 0) {
      bestIntersection = data;
      bestIntersection.vertexIndex =
          nodes[current].startIndex + 3 * bestIntersection.vertexIndex;
      intersected = true;
    }
  }
  if (!intersected) return false;
  *dat = bestIntersection;
  return true;
  // TODO(bauschp): output
}
