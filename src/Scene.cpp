// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#include <Scene.h>

#include <glm/gtx/vector_angle.hpp>
#include <random>
#include <tuple>

namespace {
std::random_device rd;
std::mt19937 generator(rd());
std::uniform_int_distribution<uint16_t> distribution(0, 0);
std::uniform_real_distribution<float> bayDist(0, 1);
}

float triangleArea(const Scene& scene, uint64_t triangleIndex) {
  auto e1 = scene.vertices[scene.indices[triangleIndex + 1]];
  auto e2 = scene.vertices[scene.indices[triangleIndex + 2]];
  auto p = scene.vertices[scene.indices[triangleIndex]];
  e1 -= p;
  e2 -= p;
  return glm::length(glm::cross(e1, e2)) / 2;
}


void listImportantThings(Scene& scene) {
  scene.importantThings.clear();
  scene.comulativeLightArea.clear();
  std::vector<uint32_t> importantMats;
  for (uint64_t matId = 0; matId < scene.materials.size(); ++matId) {
    if (glm::length(scene.materials[matId].eCol) > 0.5f)
      importantMats.push_back(matId);
  }
  float area = 0.0f;
  for (uint64_t triangleId = 0; triangleId < scene.triangleMats.size();
       ++triangleId) {
    for (auto it = importantMats.begin(); it != importantMats.end(); ++it)
      if (scene.triangleMats[triangleId] == *it) {
        scene.importantThings.push_back(triangleId * 3);
        area += triangleArea(scene, triangleId * 3);
        scene.comulativeLightArea.push_back(area);
        break;
      }
  }
  scene.totalLightArea = area;
  distribution = std::uniform_int_distribution<uint16_t>(
      0, scene.importantThings.size() - 1);
}

glm::vec3 projectToPlane(const glm::vec3& normal, const glm::vec3& other) {
  return other - glm::dot(other, normal) * other;
}

std::tuple<glm::vec3, glm::vec3, float> getRandomImportantDir(
    const Material& mat, const Scene& scene, const glm::vec3& pos,
    const glm::vec3& normal, const glm::vec3& nx, const glm::vec3& nz) {
  auto worldToLocal = glm::mat3(nx, normal, nz);
  if (scene.importantThings.size() == 0) {
    std::cout << "THAT SHOULD NOT HAPPEN!" << std::endl;
    return std::make_tuple(glm::vec3(), glm::vec3(), 0);
  }
  auto triangleIndex = scene.importantThings[distribution(generator)];
  auto e1 = scene.vertices[scene.indices[triangleIndex + 1]];
  auto e2 = scene.vertices[scene.indices[triangleIndex + 2]];
  auto p = scene.vertices[scene.indices[triangleIndex]];
  float r1 = 1;
  float r2 = 1;
  e1 -= p;
  e2 -= p;
  // generate a sample.
  while (r1 + r2 > 1) {
    r1 = bayDist(generator);
    r2 = bayDist(generator);
  }

  auto imp =
      glm::normalize((p + r1 * e1 + r2 * e2) - pos);
  return std::make_tuple(imp, imp * worldToLocal, glm::length(glm::cross(e1, e2)) / 2);
}
