// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#include <inttypes.h>
#include <algorithm>
#include <cstring>
#include <functional>
#include <iomanip>
#include <iostream>

#include <BVH.h>
#include <BiDirectionalTrace.h>
#include <Camera.h>
#include <MonteCarloTrace.h>
#include <ObjLoader.h>
#include <RayCast.h>
#include <Scene.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_angle.hpp>

using namespace std;

void postorder(const std::vector<BVHNode>& nodes, size_t current,
               int indent = 0) {
  if (indent) {
    std::cout << std::setfill('-') << std::setw(indent) << '-';
  }
  if (nodes[current].isLeaf) {
    cout << nodes[current].startIndex << "|" << nodes[current].endIndex
         << std::endl;
  } else {
    std::cout << "+" << std::endl;
    postorder(nodes, nodes[current].leftChild, indent + 1);
    postorder(nodes, nodes[current].rightChild, indent + 1);
  }
}

void load(const char* name, Scene& scene) {
  loadObj(name, scene);
  topDownBVH(scene);
  listImportantThings(scene);
}

inline float clamp(const float& lo, const float& hi, const float& v) {
  return std::max(lo, std::min(hi, v));
}

void toSRGB(const glm::vec3& pix, float gamma, unsigned char (&color)[3]) {
  const auto a = 0.055;
  for (uint8_t i = 0; i < 3u; ++i) {
    auto gammaCorrected = clamp(0, 1, powf(pix[i], 1 / gamma));
    color[i] = 255 * (gammaCorrected <= 0.0031308
                          ? 12.92 * gammaCorrected
                          : (1 + a) * powf(gammaCorrected, 1 / 2.4f) - a);
  }
}

void writeImageBufferToFile(std::vector<glm::vec3> imageBuffer, uint32_t width,
                            uint32_t height, float scale, float gamma,
                            const char* name) {
  FILE* file = fopen(name, "wb");

  if (file == nullptr) return;

  fprintf(file, "P6\n%d %d\n255\n", width, height);

  for (uint64_t j = 0; j < width * height; ++j) {
    auto pixel = imageBuffer[j] * scale;
    unsigned char color[3];
    color[0] = (255 * clamp(0, 1, powf(pixel.x, 1 / gamma)));
    color[1] = (255 * clamp(0, 1, powf(pixel.y, 1 / gamma)));
    color[2] = (255 * clamp(0, 1, powf(pixel.z, 1 / gamma)));
    toSRGB(pixel, gamma, color);
    (void)fwrite(color, 1, 3, file);
  }
  fclose(file);
}

void bvhRender(const glm::vec3& position, const Camera& cam, uint32_t width,
               uint32_t height, uint16_t numSamples, const Scene& scene) {
  const char* name = "bvhRender";
  auto invHeight = 1.0 / height;
  auto invWidth = 1.0 / width;
  uint16_t highest = 0;
  std::vector<glm::vec3> imageBuffer(width * height);
  for (uint64_t y = 0; y < height; ++y) {
    auto ndcY = (y + 0.5) * invHeight;
    for (uint64_t x = 0; x < width; ++x) {
      auto ndcX = (x + 0.5) * invWidth;
      auto dir = cam.generateDirection(ndcX, ndcY);
      uint16_t val = 0;
      countTraversedBoxes(position, dir, scene.nodes, 0, &val);
      auto col = glm::vec3(val);
      highest = std::max(val, highest);
      imageBuffer[y * width + x] = col;
    }
  }
  writeImageBufferToFile(imageBuffer, width, height, 1.0f / highest, 1, name);
}

void testRender(const glm::vec3& position, const Camera& cam, uint32_t width,
                uint32_t height, uint16_t numSamples, const Scene& scene) {
  char nameBuffer[100];
  std::random_device rd;
  std::mt19937 randomGen(rd());
  std::uniform_real_distribution<double> jitter(0, 1);
  std::vector<glm::vec3> imageBuffer(width * height);
  auto invHeight = 1.0 / height;
  auto invWidth = 1.0 / width;
  for (auto i = 1; i <= numSamples; ++i) {
    for (uint64_t y = 0; y < height; ++y) {
      auto ndcY = (y + 0.5) * invHeight;
      for (uint64_t x = 0; x < width; ++x) {
        auto ndcX = (x + 0.5) * invWidth;
        auto jitteredX = ndcX + jitter(randomGen) * invHeight;
        auto jitteredY = ndcY + jitter(randomGen) * invWidth;
        auto dir = cam.generateDirection(jitteredX, jitteredY);
        auto col = monteCarloScene(scene, position, dir);
        if (isnan(col.x) || isinf(col.x) || col.x < 0 || isnan(col.y) ||
            isinf(col.y) || col.y < 0 || isnan(col.z) || isinf(col.z) ||
            col.z < 0) {
          std::cout << "An errornous value was produces at NDC(" << jitteredX
                    << ", " << jitteredY << ")" << std::endl;
          std::cout << "Color: " << glm::to_string(col) << std::endl;
          col = glm::vec3();
        }
        imageBuffer[y * width + x] += col;
      }
    }
    // Render output.
    snprintf(nameBuffer, 100, "TestMoreSamples%02d.ppm", i);
    writeImageBufferToFile(imageBuffer, width, height, 1.0f / i, 1, nameBuffer);
  }
}

void renderLengthIntoFile(const char* name, const glm::vec3& position,
                          const Camera& cam, uint32_t width, uint32_t height,
                          uint16_t numSamples, const Scene& scene,
                          const uint32_t& s, const uint32_t& t) {
  std::cout << "Rendering with" << numSamples << "Samples." << std::endl;
  std::vector<glm::vec3> lightBuffer(width * height);
  std::vector<glm::vec3> eyeBuffer(width * height);
  std::vector<uint16_t> lightTimes(width * height);
  auto lightAccess = [&cam, &lightBuffer, &lightTimes, width, height](
      const glm::vec3& dir, const glm::vec3& contrib) {
    auto pixel = cam.toNDC(dir);
    // check if they are on the image plane.
    if (pixel.x < 0.0f || pixel.x >= 1.0f) return;
    if (pixel.y < 0.0f || pixel.y >= 1.0f) return;
    pixel *= glm::vec2(width, height);
    uint64_t px = static_cast<int>(std::floor(pixel.x));
    uint64_t py = static_cast<int>(std::floor(pixel.y));
    lightBuffer[px + width * py] += contrib;
    //std::cout << "("<< px << "," << py << ")=" << glm::to_string(contrib) << std::endl;
    lightTimes[px + width * py]++;
  };

  auto invHeight = 1.0 / height;
  auto invWidth = 1.0 / width;
  std::random_device rd;
  std::mt19937 randomGen(rd());
  auto invNumSamples = 1.0f / numSamples;
  std::uniform_real_distribution<double> jitter(0, 1);
  for (uint64_t y = 0; y < height; ++y) {
    auto ndcY = (y + 0.5) * invHeight;
    for (uint64_t x = 0; x < width; ++x) {
      auto ndcX = (x + 0.5) * invWidth;
      glm::vec3 c(0.0f);
      for (auto i = 0; i < numSamples; ++i) {
        auto jitteredX = ndcX + jitter(randomGen) * invHeight;
        auto jitteredY = ndcY + jitter(randomGen) * invWidth;
        auto dir = cam.generateDirection(jitteredX, jitteredY);
        // auto col = monteCarloScene(scene, position, dir);
        auto col =
            bdpt::bdptScene(scene, position, dir, lightAccess, s, t, true);
        if (isnan(col.x) || isinf(col.x) || col.x < 0 || isnan(col.y) ||
            isinf(col.y) || col.y < 0 || isnan(col.z) || isinf(col.z) ||
            col.z < 0) {
          std::cout << "An errornous value was produces at NDC(" << jitteredX
                    << ", " << jitteredY << ")" << std::endl;
          std::cout << "Color: " << glm::to_string(col) << std::endl;
          col = glm::vec3();
        }
        eyeBuffer[y * width + x] += invNumSamples * col;
      }
    }
  }
  if (t == 1) {
    for (auto i = 0; i < lightTimes.size(); ++i)
      lightBuffer[i] /= lightTimes[i] > 0 ? lightTimes[i] : 1.0f;
    writeImageBufferToFile(lightBuffer, width, height, 1.0f, 1, name);
  } else
    writeImageBufferToFile(eyeBuffer, width, height, invNumSamples, 1, name);
}

void renderIntoFile(const char* name, const glm::vec3& position,
                    const Camera& cam, uint32_t width, uint32_t height,
                    uint16_t numSamples, const Scene& scene) {
  std::cout << "Rendering with" << numSamples << "Samples." << std::endl;
  std::vector<glm::vec3> lightBuffer(width * height);
  std::vector<glm::vec3> eyeBuffer(width * height);
  std::vector<uint16_t> lightTimes(width * height);
  auto lightAccess = [&cam, &lightBuffer, &lightTimes, width, height](
      const glm::vec3& dir, const glm::vec3& contrib) {
    auto pixel = cam.toNDC(dir);
    // check if they are on the image plane.
    if (pixel.x < 0.0f || pixel.x >= 1.0f) return;
    if (pixel.y < 0.0f || pixel.y >= 1.0f) return;
    pixel *= glm::vec2(width, height);
    uint64_t px = static_cast<int>(std::floor(pixel.x));
    uint64_t py = static_cast<int>(std::floor(pixel.y));
    lightBuffer[px + width * py] += contrib;
    //std::cout << "("<< px << "," << py << ")=" << glm::to_string(contrib) << std::endl;
    lightTimes[px + width * py]++;
  };
  /*
  FILE* file = fopen(name, "wb");

  if (file == nullptr) return;

  fprintf(file, "P6\n%d %d\n255\n", width, height);
  */
  auto invHeight = 1.0 / height;
  auto invWidth = 1.0 / width;
  std::random_device rd;
  std::mt19937 randomGen(rd());
  auto invNumSamples = 1.0f / numSamples;
  std::uniform_real_distribution<double> jitter(0, 1);
  for (uint64_t y = 0; y < height; ++y) {
    auto ndcY = (y + 0.5) * invHeight;
    for (uint64_t x = 0; x < width; ++x) {
      auto ndcX = (x + 0.5) * invWidth;
      glm::vec3 c(0.0f);
      for (auto i = 0; i < numSamples; ++i) {
        auto jitteredX = ndcX + jitter(randomGen) * invHeight;
        auto jitteredY = ndcY + jitter(randomGen) * invWidth;
        auto dir = cam.generateDirection(jitteredX, jitteredY);
        // auto col = monteCarloScene(scene, position, dir);
        auto col =
            bdpt::bdptScene(scene, position, dir, lightAccess, 1, 1, false);
        if (isnan(col.x) || isinf(col.x) || col.x < 0 || isnan(col.y) ||
            isinf(col.y) || col.y < 0 || isnan(col.z) || isinf(col.z) ||
            col.z < 0) {
          std::cout << "An errornous value was produces at NDC(" << jitteredX
                    << ", " << jitteredY << ")" << std::endl;
          std::cout << "Color: " << glm::to_string(col) << std::endl;
          col = glm::vec3();
        }
        eyeBuffer[y * width + x] += col * invNumSamples;
      }
    }
  }
  for (auto i = 0; i < lightTimes.size(); ++i)
    lightBuffer[i] /= lightTimes[i] > 0 ? lightTimes[i] : 1.0f;
  for (auto i = 0u; i < eyeBuffer.size(); ++i) eyeBuffer[i] += lightBuffer[i];
  writeImageBufferToFile(eyeBuffer, width, height, 1.0f, 1, name);
}

void loop(Scene& scene) {
  char buffer[255];
  std::string string;
  // Camera Settings.
  glm::dvec3 axis(1.0f, 0.0f, 0.0f);
  double angle = 0.0f;
  glm::vec3 position(0.0f, 0.0f, 60.0f);
  uint16_t numSamples = 1;
  // Image settings
  uint32_t width, height;
  width = 400;
  height = 400;
  auto rotFunc = [&axis, &angle](Scene& scene) {
    std::string s;
    std::cout << "Input an axis followed by an angle: x y z angle" << std::endl;
    getline(std::cin, s);
    if (4 != sscanf(s.c_str(), "%lf %lf %lf %lf", &axis.x, &axis.y, &axis.z,
                    &angle)) {
      std::cerr << "Some input was wrong." << std::endl;
    }
    std::cout << "New axis " << glm::to_string(axis) << std::endl;
  };
  auto transFunc = [&position](Scene& scene) {
    std::string s;
    std::cout << "Input a position: x y z" << std::endl;
    getline(std::cin, s);
    if (3 !=
        sscanf(s.c_str(), "%f %f %f", &position.x, &position.y, &position.z)) {
      std::cerr << "Some input was wrong." << std::endl;
    }
  };

  auto dimFunc = [&width, &height](Scene& scene) {
    std::string s;
    std::cout << "Please input the dimensions of the image plane: dimX dimY"
              << std::endl;
    getline(std::cin, s);
    if (2 != sscanf(s.c_str(), "%" SCNu32 " %" SCNu32, &width, &height)) {
      std::cerr << "Some input was wrong." << std::endl;
    }
  };

  auto renderFunc = [&axis, &angle, &position, &width, &height,
                     &numSamples](Scene& scene) {
    std::string s;
    std::cout << "Input a filename:" << std::endl;
    getline(std::cin, s);
    char name[100];
    if (1 != sscanf(s.c_str(), "%s", &name[0])) {
      std::cerr << "Some input was wrong." << std::endl;
    }
    Camera cam(static_cast<float>(width) / height, glm::radians(90.0));
    cam.setCtVTransform(glm::rotate(glm::dmat4(), glm::radians(angle), axis));
    std::cout << "Rendering into " << name << std::endl;
    // Reset the count of triangle tests.
    numTriangleTests = 0;
    numAABBTests = 0;
    numRaysCast = 0;
    renderIntoFile(name, position, cam, width, height, numSamples, scene);
    std::cout << "(Triangle tests|AABB tests|numRaysCast) = ("
              << numTriangleTests << "|" << numAABBTests << "|" << numRaysCast
              << ")" << std::endl;
  };

  auto renderLengthFunc = [&axis, &angle, &position, &width, &height,
                           &numSamples](Scene& scene) {
    std::string s;
    std::cout << "Input Desired Path (s,t):" << std::endl;
    getline(std::cin, s);
    uint32_t oS, t;
    if (2 != sscanf(s.c_str(), "%" SCNu32 " %" SCNu32, &oS, &t)) {
      std::cerr << "Some input was wrong." << std::endl;
      return;
    }
    std::cout << "Input a filename:" << std::endl;
    getline(std::cin, s);
    char name[100];
    if (1 != sscanf(s.c_str(), "%s", &name[0])) {
      std::cerr << "Some input was wrong." << std::endl;
      return;
    }
    Camera cam(static_cast<float>(width) / height, glm::radians(90.0));
    cam.setCtVTransform(glm::rotate(glm::dmat4(), glm::radians(angle), axis));
    std::cout << "Rendering into " << name << std::endl;
    // Reset the count of triangle tests.
    numTriangleTests = 0;
    numAABBTests = 0;
    numRaysCast = 0;
    renderLengthIntoFile(name, position, cam, width, height, numSamples, scene,
                         oS, t);
    std::cout << "(Triangle tests|AABB tests|numRaysCast) = ("
              << numTriangleTests << "|" << numAABBTests << "|" << numRaysCast
              << ")" << std::endl;
  };

  auto loadFunc = [](Scene& scene) {
    std::string s;
    std::cout << "Input a filename:" << std::endl;
    getline(std::cin, s);
    char name[200];
    if (1 != sscanf(s.c_str(), "%s", &name[0])) {
      std::cerr << "Some input was wrong." << std::endl;
    }
    load(name, scene);
  };
  auto samplesFunc = [&numSamples](Scene& scene) {
    std::string s;
    std::cout << "Input value:" << std::endl;
    getline(std::cin, s);
    if (1 != sscanf(s.c_str(), "%" SCNu16, &numSamples)) {
      std::cerr << "Some input was wrong." << std::endl;
    }
  };
  auto backgroundFunc = [](Scene& scene) {
    std::string s;
    std::cout << "Input values: r g b" << std::endl;
    getline(std::cin, s);
    if (3 != sscanf(s.c_str(), "%f %f %f", &backGround.r, &backGround.g,
                    &backGround.b)) {
      std::cerr << "Some input was wrong." << std::endl;
    }
  };

  auto testRenderFunc = [&axis, &angle, &position, &width, &height,
                         &numSamples](Scene& scene) {
    Camera cam(static_cast<float>(width) / height, glm::radians(90.0));
    cam.setCtVTransform(glm::rotate(glm::dmat4(), glm::radians(angle), axis));
    testRender(position, cam, width, height, numSamples, scene);
  };

  auto bvhRenderFunc = [&axis, &angle, &position, &width, &height,
                        &numSamples](Scene& scene) {
    Camera cam(static_cast<float>(width) / height, glm::radians(90.0));
    cam.setCtVTransform(glm::rotate(glm::dmat4(), glm::radians(angle), axis));
    bvhRender(position, cam, width, height, numSamples, scene);
  };

  std::vector<std::string> options = {
      "rotate",     "translate", "dimensions", "render", "load",
      "numSamples", "bg",        "testrender", "bvh",    "plengthRender"};
  std::vector<std::function<void(Scene&)> > calls{
      rotFunc,       transFunc,       dimFunc,        renderFunc,
      loadFunc,      samplesFunc,     backgroundFunc, testRenderFunc,
      bvhRenderFunc, renderLengthFunc};
  std::cout << "What's next?" << std::endl;
  while (getline(std::cin, string)) {
    int matches = sscanf(string.c_str(), "%s", buffer);
    if (matches != 1) continue;
    for (uint8_t i = 0; i < options.size(); ++i) {
      if (0 == strcmp(buffer, options[i].c_str())) {
        calls[i](scene);
        break;
      }
    }
    if (0 == strcmp(buffer, "exit")) {
      break;
    }
    std::cout << "What's next? (";
    std::for_each(options.begin(), options.end(),
                  [](const auto& val) { std::cout << val << ","; });
    std::cout << "exit)" << std::endl;
  }
}

int main() {
  Scene scene;
  loop(scene);
  return 0;
}
