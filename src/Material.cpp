// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#include <Material.h>
#include <random>
#include <stack>
#include <tuple>
#include <utility>

#include <glm/glm.hpp>

namespace {
std::random_device rd;
std::mt19937 generator(rd());
std::uniform_real_distribution<float> distribution(0, 1);
// Specular = 0; Refract = 1;
std::uniform_int_distribution<uint16_t> whichDir(0, 1);
}

std::tuple<float, float, glm::vec3, float, glm::vec3> generateSampleForMaterial(
    const Material& mat, const glm::vec3& incident, const glm::vec3& normal,
    std::stack<float>& ior) {
  // select the right sampling method.
  if (mat.isTransparent) {
    // Transparent.
    auto t1 = generateTransparentSamples(mat, incident, normal, ior);
    return std::tuple_cat(
        t1,
        std::make_tuple(transparentPDF(std::get<0>(t1), std::get<1>(t1)),
                        evalMaterial(mat, std::get<2>(t1), incident, normal)));
  } else {
    // Diffuse
    glm::vec3 nx;
    glm::vec3 nz;
    createCoordinateSystem(normal, nx, nz);
    auto t1 = generateCosWeightedHemSamples();
    auto worldSample = hemisphereToWorld(normal, nx, nz, std::get<2>(t1));
    return std::make_tuple(
        std::get<0>(t1), std::get<1>(t1), worldSample,
        cosWeightedPDF(std::get<0>(t1), std::get<1>(t1)),
        evalMaterial(mat, std::get<2>(t1), incident, normal));
  }
}

std::tuple<float, float, glm::vec3> generateTransparentSamples(
    const Material& mat, const glm::vec3& incident, const glm::vec3& normal,
    std::stack<float>& ior) {
  auto cosTheta = glm::dot(normal, incident);
  if (whichDir(generator) == 0) {
    // Specular the normal can be fliped while still resulting in the same
    // direction.
    auto reflectedDir = 2 * glm::dot(normal, incident) * normal - incident;
    return std::make_tuple(M_PI, acosf(cosTheta), reflectedDir);
  } else {
    // Refracted In -> Out?
    auto refNormal = normal;
    auto n1 = 1.0f;
    auto n2 = 1.0f;
    if (cosTheta <= 0) {
      refNormal = -normal;
      cosTheta = glm::dot(incident, refNormal);
      // TODO(bauschp): Rays might start in something other than air.
      if (!ior.empty()) {
        n1 = ior.top();
        ior.pop();
      }
      n2 = !ior.empty() ? ior.top() : 1.0f;
    } else {
      if (!ior.empty()) n1 = ior.top();
      // Push the new ior onto the stack.
      ior.push(mat.ior);
      n2 = mat.ior;
    }
    // SNELL: sin(theta_1)/sin(theta_2)=n_2/n_1
    auto angleR = asinf(sinf(acosf(cosTheta)) * (n1 / n2));
    auto ratio = n1 / n2;
    // Heckbert
    auto refractedDir =
        (ratio * cosTheta - cosf(angleR)) * refNormal - ratio * incident;
    return std::make_tuple(M_PI, M_PI - angleR, refractedDir);
  }
}

float transparentPDF(float phi, float theta) { return 0.5f; }

void createCoordinateSystem(const glm::vec3& N, glm::vec3& Nt, glm::vec3& Nb) {
  if (std::fabs(N.x) > std::fabs(N.y))
    Nt = glm::vec3(N.z, 0, -N.x) / sqrtf(N.x * N.x + N.z * N.z);
  else
    Nt = glm::vec3(0, -N.z, N.y) / sqrtf(N.y * N.y + N.z * N.z);
  Nb = glm::cross(N, Nt);
}

glm::vec3 uniformHemisphereSpaceSample(float r1, float r2) {
  float sinTheta = sqrtf(1 - r1 * r1);
  float phi = 2 * M_PI * r2;
  float x = sinTheta * cosf(phi);
  float z = sinTheta * sinf(phi);
  return glm::vec3(x, r1, z);
}

std::tuple<float, float, glm::vec3> generateCosWeightedHemSamples() {
  auto r1 = distribution(generator);
  auto r2 = distribution(generator);
  auto theta = -asinf(sqrtf(2 * r1 / M_PI));
  auto phi = 2 * M_PI * r2;
  auto sinTheta = sinf(theta);
  auto cosTheta = cosf(theta);
  return std::make_tuple(phi, theta, glm::vec3(sinTheta * cosf(phi), cosTheta,
                                               sinTheta * sinf(phi)));
}

float cosWeightedPDF(float phi, float theta) { return (cosf(theta)) / (M_PI); }

glm::vec3 hemisphereToWorld(const glm::vec3& N, const glm::vec3& Nt,
                            const glm::vec3& Nb, const glm::vec3& hemSample) {
  return glm::vec3(hemSample.x * Nb.x + hemSample.y * N.x + hemSample.z * Nt.x,
                   hemSample.x * Nb.y + hemSample.y * N.y + hemSample.z * Nt.y,
                   hemSample.x * Nb.z + hemSample.y * N.z + hemSample.z * Nt.z);
}

/// Returnes the ammout of light that will be transported.
glm::vec3 evalMaterial(const Material& mat, const glm::vec3& omegaI,
                       const glm::vec3& omegaO, const glm::vec3& normal) {
  if (mat.isTransparent) {
    auto cosTheta = glm::dot(normal, omegaO);

    return cosTheta < 0 ? mat.dCol * 0.2f : mat.sCol;
  }
  return mat.dCol / static_cast<float>(M_PI);
}
