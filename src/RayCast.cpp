// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#include <RayCast.h>

#include <glm/glm.hpp>

uint64_t numTriangleTests = 0;

bool rayCast(const glm::vec3& origin, const glm::vec3& dir,
             const glm::vec3* vertices, float* const t) {
  auto e1 = vertices[1] - vertices[0];
  auto e2 = vertices[2] - vertices[0];
  auto p = glm::cross(dir, e2);
  auto k = glm::dot(e1, p);
  if (k > -EPSILON && k < EPSILON) return false;
  auto f = 1 / k;
  auto s = origin - vertices[0];
  auto a = f * glm::dot(s, p);
  if (a < 0 || a > 1) return false;
  auto q = glm::cross(s, e1);
  auto b = f * glm::dot(dir, q);
  if (b < 0 || b > 1) return false;
  *t = f * glm::dot(e2, q);
  return true;
}
