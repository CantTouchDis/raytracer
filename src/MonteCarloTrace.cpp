// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#include <MonteCarloTrace.h>

#include <BRDF.h>
#include <BVH.h>
#include <Material.h>
#include <RayCast.h>
#include <Scene.h>
#include <algorithm>
#include <glm/gtx/vector_angle.hpp>
#include <stack>
#include <tuple>

#include <random>

float ep = 1e-4;
uint32_t maxDepth = 10u;

namespace {
std::random_device rd;
std::mt19937 generator(rd());
std::uniform_real_distribution<float> bayDist(0, 1);

struct PathNode {
  glm::vec3 pos;
  glm::vec3 normal;
  uint32_t matId;
  bool onLight;
};

struct ValueNode {
  float comulativeProb;
  glm::vec3 comulativeAlpha;
};

inline bool isLight(const BRDFData& mat) { return glm::length2(mat.eCol) > 0; }

inline glm::vec3 calcNormal(const Scene& scene, uint64_t triangleIndex,
                            float b1, float b2) {
  return static_cast<float>(1 - b1 - b2) *
             scene.normals[scene.normalIndices[triangleIndex]] +
         b1 * scene.normals[scene.normalIndices[triangleIndex + 1]] +
         b2 * scene.normals[scene.normalIndices[triangleIndex + 2]];
}
inline float geom(const glm::vec3& n1, const glm::vec3& n2,
                  const glm::vec3& dir21, float length2) {
  return (glm::dot(n1, -dir21) * glm::dot(n2, dir21)) / length2;
}

uint16_t generatePath(const Scene& scene, const glm::vec3& origin,
                      glm::vec3& direction, uint16_t maxSubpathLength,
                      std::vector<PathNode>* pathNodes,
                      std::vector<ValueNode>* valueNodes, bool light) {
  glm::vec3 nextDir;
  auto comulativeProb = 1.0f;
  auto comulativeAlpha = glm::vec3(1.0f);
  if (!light) {
    pathNodes->push_back({origin, direction, static_cast<uint32_t>(-1), false});
    valueNodes->push_back({comulativeProb, comulativeAlpha});
    nextDir = direction;
    comulativeAlpha *= 1;
    comulativeProb /= 1;
  } else {
    // Find a light
    auto randomArea = bayDist(generator) * scene.totalLightArea;
    auto it = std::lower_bound(scene.comulativeLightArea.begin(),
                               scene.comulativeLightArea.end(), randomArea);
    if (it == scene.comulativeLightArea.end()) return 0u;
    auto triangleIndex = scene.importantThings[std::distance(
        scene.comulativeLightArea.begin(), it)];
    auto e1 = scene.vertices[scene.indices[triangleIndex + 1]];
    auto e2 = scene.vertices[scene.indices[triangleIndex + 2]];
    auto p = scene.vertices[scene.indices[triangleIndex]];
    e1 -= p;
    e2 -= p;
    float r1 = 1;
    float r2 = 1;
    // generate a sample on this triangle.
    while (r1 + r2 > 1) {
      r1 = bayDist(generator);
      r2 = bayDist(generator);
    }
    auto lPos = p + r1 * e1 + r2 * e2;
    auto lNormal = calcNormal(scene, triangleIndex, r1, r2);
    const auto mat = scene.triangleMats[triangleIndex / 3];
    pathNodes->push_back({lPos, lNormal, mat, true});
    valueNodes->push_back({comulativeProb, comulativeAlpha});
    comulativeAlpha = scene.materials[mat].eCol * scene.totalLightArea;
    nextDir = lNormal;
    comulativeProb /= scene.totalLightArea;
  }
  int32_t currentlyTraversedMaterial = -1;
  for (uint16_t i = 1; i <= maxSubpathLength; ++i) {
    const auto& currentVertex = pathNodes->back();
    BRDFData::sampleInfo sample;
    if (currentVertex.matId == static_cast<uint32_t>(-1)) {
      // if there is no material follow in normal direction with probability 1.
      sample = std::make_tuple(nextDir, glm::vec3(1.0f), 1.0f);
      // Visibility is guarantied.
    } else {
      const auto& mat = scene.materials[currentVertex.matId];
      int32_t nextTraversedMaterial = currentVertex.matId;
      sample = mat.generateSampleFunc(mat, -nextDir, currentVertex.normal,
                                      currentlyTraversedMaterial,
                                      nextTraversedMaterial);
      currentlyTraversedMaterial = nextTraversedMaterial;
    }
    nextDir = std::get<0>(sample);
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    bool hit =
        traverseBVH(currentVertex.pos + ep * nextDir, nextDir, scene.vertices,
                    scene.indices, scene.nodes, 0, &data);
    if (!hit) break;
    auto hitPos = currentVertex.pos + static_cast<float>(data.t) * nextDir;
    auto normal = calcNormal(scene, data.vertexIndex, data.a, data.b);
    pathNodes->push_back(
        {hitPos, normal, scene.triangleMats[data.vertexIndex / 3],
         isLight(scene.materials[scene.triangleMats[data.vertexIndex / 3]])});
    valueNodes->push_back({comulativeProb, comulativeAlpha});
    // next Prob
    comulativeProb *=
        geom(currentVertex.normal, normal, -nextDir, data.t * data.t) *
        std::get<2>(sample);
    // next alpha
    comulativeAlpha *= std::get<1>(sample) / std::get<2>(sample);
    // next omega
    nextDir = std::get<0>(sample);
  }
  valueNodes->push_back({comulativeProb, comulativeAlpha});
  return pathNodes->size();
}
}

inline glm::vec3 bdptPath(const Scene& scene,
                          std::vector<PathNode>& eyeVertices,
                          std::vector<ValueNode>& eyeValues,
                          std::vector<PathNode>& lightVertices,
                          std::vector<ValueNode>& lightValues, writeToLightFunc lBuff, uint32_t s,
                          uint32_t t) {
  auto numLightVerts = lightVertices.size();
  auto numEyeVerts = eyeVertices.size();
  // s many light Vertices + t many eye vertices give a path of length s + t
  // if any of these values is out of bounds skip it. Also skip t = 0 cause
  // this
  // implementation dosn't allow for these paths to exist.
  if (s >= numLightVerts) return glm::vec3(0.0f);
  if (t >= numEyeVerts || t <= 0) return glm::vec3(0.0f);
  auto weight = 1.0f;
  glm::vec3 connection(0.0f);
  if (s == 0) {
    const auto& mat = scene.materials[eyeVertices[t - 1].matId];
    if (isLight(mat)) {
      connection = mat.eCol;
    }
  } else if (t == 1) {
    // Direct connection to the cam.
    const auto& eyeVertex = eyeVertices[t - 1];
    const auto& lightVertex = lightVertices[s - 1];
    auto connectionDirection = eyeVertex.pos - lightVertex.pos;
    auto distance2 = glm::length2(connectionDirection);
    connectionDirection = glm::normalize(connectionDirection);
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    bool canSee = true;
    if (traverseBVH(lightVertex.pos + ep * connectionDirection,
                    connectionDirection, scene.vertices, scene.indices,
                    scene.nodes, 0, &data)) {
      canSee = data.t * data.t > distance2;
    }
    if (canSee) {
      const auto& lightMat = scene.materials[lightVertex.matId];
      auto from = s >= 2 ? lightVertices[s - 2].pos - lightVertex.pos : connectionDirection;
      auto fl = lightMat.evalBRDF != nullptr
                    ? lightMat.evalBRDF(lightMat, from,
                                        connectionDirection, lightVertex.normal)
                    : glm::vec3(0.0f);
      auto g = geom(-connectionDirection, lightVertex.normal,
                    connectionDirection, distance2);
      lBuff(-connectionDirection, (lightValues[s].comulativeAlpha * fl * g) * weight);
    }
  } else {
    const auto& eyeVertex = eyeVertices[t - 1];
    const auto& lightVertex = lightVertices[s - 1];
    const auto& lightMat = scene.materials[lightVertex.matId];
    const auto& eyeMat = scene.materials[eyeVertex.matId];
    // Check if they see each other.
    auto connectionDirection = eyeVertex.pos - lightVertex.pos;
    auto distance2 = glm::length2(connectionDirection);
    connectionDirection = glm::normalize(connectionDirection);
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    if (!traverseBVH(lightVertex.pos + ep * connectionDirection,
                     connectionDirection, scene.vertices, scene.indices,
                     scene.nodes, 0, &data))
      return glm::vec3(0.0f);
    // if they do compute the connection terms.
    auto fl = lightMat.evalBRDF != nullptr
                  ? lightMat.evalBRDF(lightMat, connectionDirection,
                                      connectionDirection, lightVertex.normal)
                  : glm::vec3(0.0f);
    if (lightVertex.onLight) fl = glm::vec3(1.0f);
    auto g = geom(eyeVertex.normal, lightVertex.normal, connectionDirection,
                  distance2);
    if (g < 0.0f) std::cout << "Geom term < 0 confirmed." << std::endl;
    auto fr = glm::vec3(1.0f);
    if (eyeVertex.matId != static_cast<uint32_t>(-1))
      fr = eyeMat.evalBRDF != nullptr
               ? eyeMat.evalBRDF(eyeMat, -connectionDirection,
                                 -connectionDirection, eyeVertex.normal)
               : glm::vec3(0.0f);
    connection = fl * g * fr;
  }

  auto contribution = eyeValues[t].comulativeAlpha *
                      lightValues[s].comulativeAlpha * connection;
  return contribution * weight;
}

//glm::vec3 bdptScene(const Scene& scene, const glm::vec3& origin,
//                    glm::vec3& direction, writeToLightFunc lBuff, uint16_t oS,
//                    uint16_t oT, bool onlyLength) {
//  // Calculate a sub-path from the camera.
//  uint16_t maxSubpathLength = 5;
//  std::vector<PathNode> eyeVertices;
//  std::vector<ValueNode> eyeValues;
//  uint16_t numEyeVerts =
//      generatePath(scene, origin, direction, maxSubpathLength, &eyeVertices,
//                   &eyeValues, false);
//  std::vector<PathNode> lightVertices;
//  std::vector<ValueNode> lightValues;
//  uint16_t numLightVerts =
//      generatePath(scene, origin, direction, maxSubpathLength, &lightVertices,
//                   &lightValues, true);
//  // if (numEyeVerts > 1 && eyeVertices[1].onLight) return glm::vec3(1.0f);
//  uint16_t maxPathLength =
//      std::min<uint16_t>(numEyeVerts + numLightVerts - 1, maxDepth);
//  if (onlyLength) {
//    return bdptPath(scene, eyeVertices, eyeValues, lightVertices, lightValues, lBuff,
//                    oS, oT);
//  }
//  glm::vec3 out(0.0f);
//  for (uint16_t k = 1; k <= maxPathLength; ++k) {
//    for (uint16_t s = 0; s <= k + 1; ++s) {
//      // s many light Vertices + t many eye vertices give a path of length s + t
//      // - 1 = k
//      auto t = k - s + 1;
//      // if any of these values is out of bounds skip it. Also skip t = 0 cause
//      // this
//      // implementation dosn't allow for these paths to exist.
//      if (s >= numLightVerts) break;
//      if (t >= numEyeVerts || t <= 0) continue;
//      glm::vec3 connection(0.0f);
//      if (s == 0) {
//        const auto& mat = scene.materials[eyeVertices[t - 1].matId];
//        if (isLight(mat)) {
//          connection = mat.eCol;
//        }

//      } else if (t == 1) {
//        // Direct connection to the cam.
//        const auto& eyeVertex = eyeVertices[t - 1];
//        const auto& lightVertex = lightVertices[s - 1];

//        auto connectionDirection = eyeVertex.pos - lightVertex.pos;
//        auto distance2 = glm::length2(connectionDirection);
//        connectionDirection = glm::normalize(connectionDirection);
//        IntersectionData data;
//        data.t = std::numeric_limits<double>::max();
//        bool canSee = true;
//        if (traverseBVH(lightVertex.pos + ep * connectionDirection,
//                        connectionDirection, scene.vertices, scene.indices,
//                        scene.nodes, 0, &data)) {
//          canSee = data.t * data.t > distance2;
//        }
//        if (canSee) {
//          const auto& lightMat = scene.materials[lightVertex.matId];
//          auto from = s >= 2 ? lightVertices[s - 2].pos - lightVertex.pos : connectionDirection;
//          auto fl =
//              lightMat.evalBRDF != nullptr
//                  ? lightMat.evalBRDF(lightMat, from,
//                                      connectionDirection, lightVertex.normal)
//                  : glm::vec3(0.0f);
//          auto g = geom(-connectionDirection, lightVertex.normal,
//                        connectionDirection, distance2);
//          lBuff(-connectionDirection, lightValues[s].comulativeAlpha * fl * g);
//        }
//      } else {
//        const auto& eyeVertex = eyeVertices[t - 1];
//        const auto& lightVertex = lightVertices[s - 1];
//        const auto& lightMat = scene.materials[lightVertex.matId];
//        const auto& eyeMat = scene.materials[eyeVertex.matId];
//        // Check if they see each other.
//        auto connectionDirection = eyeVertex.pos - lightVertex.pos;
//        auto distance2 = glm::length2(connectionDirection);
//        connectionDirection = glm::normalize(connectionDirection);
//        IntersectionData data;
//        data.t = std::numeric_limits<double>::max();
//        if (!traverseBVH(lightVertex.pos + ep * connectionDirection,
//                         connectionDirection, scene.vertices, scene.indices,
//                         scene.nodes, 0, &data))
//          continue;
//        // if they do compute the connection terms.
//        auto fl =
//            lightMat.evalBRDF != nullptr
//                ? lightMat.evalBRDF(lightMat, connectionDirection,
//                                    connectionDirection, lightVertex.normal)
//                : glm::vec3(0.0f);
//        auto g = geom(eyeVertex.normal, lightVertex.normal, connectionDirection,
//                      distance2);
//        if (g < 0.0f) std::cout << "Geom term < 0 confirmed." << std::endl;
//        auto fr = glm::vec3(1.0f);
//        if (eyeVertex.matId != static_cast<uint32_t>(-1))
//          fr = eyeMat.evalBRDF != nullptr
//                   ? eyeMat.evalBRDF(eyeMat, -connectionDirection,
//                                     -connectionDirection, eyeVertex.normal)
//                   : glm::vec3(0.0f);
//        connection = fl * g * fr;
//      }

//      auto contribution = eyeValues[t].comulativeAlpha *
//                          lightValues[s].comulativeAlpha * connection;
//      out += 1.0f / (s + t + 1) * contribution;
//    }
//  }
//  /*
//  // auto k = 4u;
//  // Loop over all possible path lengths and connect. with i light vertices.
//  for (uint16_t k = 1; k <= maxPathLength; ++k) {
//    auto sum = 0.0f;
//    for (uint16_t s = 1; s <= std::min<uint32_t>(numLightVerts, k) - 1; ++s) {
//      auto t = k - s + 1;
//      const auto& eyeVertex = eyeVertices[t - 1];
//      const auto& lightVertex = lightVertices[s - 1];
//      sum += lightVertex.comulativeProb * eyeVertex.comulativeProb;
//    }
//    for (uint16_t s = 0; s <= std::min<uint32_t>(numLightVerts, k); ++s) {
//      auto t = k - s + 1;
//      if (t > eyeVertices.size() || t == 0) continue;
//      auto prob = 0.0f;
//      auto contribution = glm::vec3(0.0f);
//      // ATTENTION!!!! The following two could be invalid.
//      // Only access after conditions on s and t
//      const auto& eyeVertex = eyeVertices[t - 1];
//      const auto& lightVertex = lightVertices[s - 1];
//      if (s == 0) {
//        // Zero Light Subpath
//        prob = eyeVertex.comulativeProb;
//        // check if the last eyeVertex is a light.

//        if (eyeVertex.onLight) {
//          // std::cout << "t=" << t << ", Light" << std::endl;
//          contribution =
//              eyeVertex.comulativeAlpha * scene.materials[eyeVertex.matId].eCol;
//        }
//      } else {
//        // if (s == 1) continue;
//        // s > 0 & t > 0
//        contribution = lightVertex.comulativeAlpha * eyeVertex.comulativeAlpha;
//        prob = lightVertex.comulativeProb * eyeVertex.comulativeProb;
//        auto connectionDirection = eyeVertex.pos - lightVertex.pos;
//        auto distance2 = glm::length2(connectionDirection);
//        connectionDirection = glm::normalize(connectionDirection);
//        IntersectionData data;
//        data.t = std::numeric_limits<double>::max();
//        // Check if the two connecting nodes are visible to each other.
//        if (traverseBVH(lightVertex.pos + ep * connectionDirection,
//                        connectionDirection, scene.vertices, scene.indices,
//                        scene.nodes, 0, &data)) {
//          const auto& lightMat = scene.materials[lightVertex.matId];
//          const auto& eyeMat = scene.materials[eyeVertex.matId];

//          auto fl =
//              lightMat.evalBRDF != nullptr
//                  ? lightMat.evalBRDF(lightMat, connectionDirection,
//                                      connectionDirection, lightVertex.normal)
//                  : glm::vec3(0.0f);
//          auto g = geom(eyeVertex.normal, lightVertex.normal,
//                        connectionDirection, distance2);
//          auto fr = glm::vec3(1.0f);
//          if (eyeVertex.matId != static_cast<uint32_t>(-1))
//            fr = eyeMat.evalBRDF != nullptr
//                     ? eyeMat.evalBRDF(eyeMat, -connectionDirection,
//                                       -connectionDirection, eyeVertex.normal)
//                     : glm::vec3(0.0f);
//          contribution *= fl * g * fr;
//        } else
//          // Not visible? try next.
//          continue;
//        if (s == 1) {
//          // One Light Vertex
//        }
//      }
//      out += (prob / sum) * contribution;
//    }
//  }
//  */
//  return out;
//}


#ifdef BRDF
glm::vec3 monteCarloScene(const Scene& scene, glm::vec3 origin,
                          glm::vec3 direction) {
  glm::vec3 outColor(0.0f);
  // absorbtion, origin, direction, depth, allow emissive.
  std::stack<std::tuple<glm::vec3, glm::vec3, glm::vec3, uint8_t, bool> > stack;
  stack.push(std::make_tuple(glm::vec3(1.0f), origin, direction, 0, true));
  // std::stack<float> ior;
  // -1 for air.
  int32_t currentlyTraversedMaterial = -1;
  while (!stack.empty()) {
    auto top = stack.top();
    stack.pop();
    auto absorbtion = std::get<0>(top);
    auto orig = std::get<1>(top);
    auto dir = std::get<2>(top);
    auto currentDepth = std::get<3>(top);
    auto emissiveAllowed = std::get<4>(top);

    if (currentDepth >= maxDepth || glm::length(absorbtion) < 0.001f) continue;
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    bool hit = traverseBVH(orig, dir, scene.vertices, scene.indices,
                           scene.nodes, 0, &data);
    // if we don't hit anything. Stop tracing and return
    if (!hit) {
      outColor += absorbtion * backGround;
      continue;
    }
    int32_t nextTraversedMaterial = scene.triangleMats[data.vertexIndex / 3];
    // The material.
    const auto& material = scene.materials[nextTraversedMaterial];
    if (emissiveAllowed) outColor += absorbtion * material.eCol;
    // Calculate the normal
    auto normal = static_cast<float>(1 - data.a - data.b) *
                      scene.normals[scene.normalIndices[data.vertexIndex]] +
                  static_cast<float>(data.a) *
                      scene.normals[scene.normalIndices[data.vertexIndex + 1]] +
                  static_cast<float>(data.b) *
                      scene.normals[scene.normalIndices[data.vertexIndex + 2]];
    normal = glm::normalize(normal);
    // turn the ray direction around to use it in the BRDF functions.
    auto omegaO = glm::normalize(-dir);
    auto hitPos = orig + static_cast<float>(data.t) * dir;
    // are direct shadow rays are allowed?
    if (material.evalBRDF != nullptr) {
      // Send to every important thing.
      for (auto it = scene.importantThings.begin();
           it != scene.importantThings.end(); ++it) {
        auto triangleIndex = *it;
        auto e1 = scene.vertices[scene.indices[triangleIndex + 1]];
        auto e2 = scene.vertices[scene.indices[triangleIndex + 2]];
        auto p = scene.vertices[scene.indices[triangleIndex]];
        float r1 = 1;
        float r2 = 1;
        e1 -= p;
        e2 -= p;
        // reject samples above bay-coords
        while (r1 + r2 > 1) {
          r1 = bayDist(generator);
          r2 = bayDist(generator);
        }
        auto posOnLight = (p + r1 * e1 + r2 * e2);
        auto lDir = posOnLight - hitPos;
        auto expectedT = glm::length(lDir);
        lDir = glm::normalize(lDir);
        // Check if light is visible.
        IntersectionData directCheck;
        directCheck.t = std::numeric_limits<double>::max();
        bool hitDirect =
            traverseBVH(hitPos + ep * lDir, lDir, scene.vertices, scene.indices,
                        scene.nodes, 0, &directCheck);
        // if something was hit and it was our light.
        // visibility  && fabs(expectedT - directCheck.t) < 0.1f
        if (hitDirect && fabs(expectedT - directCheck.t) < 0.1f) {
          auto area = glm::length(glm::cross(e1, e2)) / 2;
          auto lightNormal =
              static_cast<float>(1 - directCheck.a - directCheck.b) *
                  scene.normals[scene.normalIndices[directCheck.vertexIndex]] +
              static_cast<float>(directCheck.a) *
                  scene.normals[scene.normalIndices[directCheck.vertexIndex +
                                                    1]] +
              static_cast<float>(directCheck.b) *
                  scene
                      .normals[scene.normalIndices[directCheck.vertexIndex + 2]];
          auto g = std::max<float>(0.0, glm::dot(lDir, normal) *
                                            glm::dot(-lDir, lightNormal)) /
                   static_cast<float>(directCheck.t * directCheck.t);
          // add the light to our output
          outColor +=
              absorbtion * area * g *
              material.evalBRDF(material, lDir, omegaO, normal) *
              scene.materials[scene.triangleMats[directCheck.vertexIndex / 3]]
                  .eCol;
        }
      }
    }
    // BRDF sampled indirect.
    auto sample = material.generateSampleFunc(material, omegaO, normal,
                                              currentlyTraversedMaterial,
                                              nextTraversedMaterial);
    // Absorption without transmission computation.
    auto invPDF =
        fabs(std::get<2>(sample)) < 1e-4 ? 0.0f : 1.0f / std::get<2>(sample);
    auto newAbsorb = absorbtion * glm::dot(std::get<0>(sample), normal) *
                     std::get<1>(sample) * invPDF;
    // compute the absorption by the material we are traversing.
    if (currentlyTraversedMaterial >= 0) {
      const auto& inMaterial = scene.materials[currentlyTraversedMaterial];
      newAbsorb *= lambertTransmission(inMaterial, data.t);
    } else {
    }
    currentlyTraversedMaterial = nextTraversedMaterial;
    stack.push(std::make_tuple(newAbsorb, hitPos + ep * std::get<0>(sample),
                               std::get<0>(sample), currentDepth + 1,
                               material.evalBRDF == nullptr));
  }
  return outColor;
}

#else
glm::vec3 monteCarloScene(const Scene& scene, glm::vec3 origin,
                          glm::vec3 direction) {
  glm::vec3 outColor(0.0f);

  std::stack<std::tuple<glm::vec3, glm::vec3, glm::vec3, uint8_t> > stack;
  stack.push(std::make_tuple(glm::vec3(1.0f), origin, direction, 0));
  std::stack<float> ior;
  // Iterate until we reached the maximum depth.
  const uint8_t maxDepth = 10;
  while (!stack.empty()) {
    auto top = stack.top();
    stack.pop();
    auto absorbtion = std::get<0>(top);
    auto orig = std::get<1>(top);
    auto dir = std::get<2>(top);
    auto currentDepth = std::get<3>(top);
    if (currentDepth >= maxDepth || glm::length(absorbtion) < 0.001f) continue;
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    bool hit = traverseBVH(orig, dir, scene.vertices, scene.indices,
                           scene.nodes, 0, &data);
    // if we don't hit anything. Stop tracing and return
    if (!hit) {
      outColor += absorbtion * backGround;
      continue;
    }
    // The material.
    const auto& material =
        scene.materials[scene.triangleMats[data.triangleId / 3]];

    // Add the emitted color to the output if its a camera Ray.
    if (currentDepth == 0) outColor += absorbtion * material.eCol;
    // if (glm::length(material.eCol) > 0.5f) continue;

    // Calculate the normal
    auto normal = static_cast<float>(1 - data.a - data.b) *
                      scene.normals[scene.normalIndices[data.triangleId]] +
                  static_cast<float>(data.a) *
                      scene.normals[scene.normalIndices[data.triangleId + 1]] +
                  static_cast<float>(data.b) *
                      scene.normals[scene.normalIndices[data.triangleId + 2]];
    normal = glm::normalize(normal);
    // turn the ray direction around to use it in the BRDF functions.
    auto omegaO = glm::normalize(-dir);
    auto hitPos = orig + static_cast<float>(data.t) * dir;

    // Now we split up possible direct and indirect light.
    // Direct
    if (scene.importantThings.size() != 0 && !material.isTransparent) {
      // Send to every important thing.
      for (auto it = scene.importantThings.begin();
           it != scene.importantThings.end(); ++it) {
        auto triangleIndex = *it;
        auto e1 = scene.vertices[scene.indices[triangleIndex + 1]];
        auto e2 = scene.vertices[scene.indices[triangleIndex + 2]];
        auto p = scene.vertices[scene.indices[triangleIndex]];
        float r1 = 1;
        float r2 = 1;
        e1 -= p;
        e2 -= p;
        // reject samples above bay-coords
        while (r1 + r2 > 1) {
          r1 = bayDist(generator);
          r2 = bayDist(generator);
        }
        auto posOnLight = (p + r1 * e1 + r2 * e2);
        auto lDir = posOnLight - hitPos;
        auto expectedT = glm::length2(lDir);
        lDir = glm::normalize(lDir);
        // Check if light is visible.
        IntersectionData directCheck;
        directCheck.t = std::numeric_limits<double>::max();
        bool hitDirect =
            traverseBVH(hitPos + ep * lDir, lDir, scene.vertices, scene.indices,
                        scene.nodes, 0, &directCheck);
        // if something was hit and it was our light.
        // visibility  && fabs(expectedT - directCheck.t) < 0.1f
        if (hitDirect) {
          auto area = glm::length(glm::cross(e1, e2)) / 2;
          auto lightNormal =
              static_cast<float>(1 - directCheck.a - directCheck.b) *
                  scene.normals[scene.normalIndices[directCheck.triangleId]] +
              static_cast<float>(directCheck.a) *
                  scene.normals[scene.normalIndices[directCheck.triangleId +
                                                    1]] +
              static_cast<float>(directCheck.b) *
                  scene
                      .normals[scene.normalIndices[directCheck.triangleId + 2]];
          auto g = std::max<float>(0.0, glm::dot(lDir, normal) *
                                            glm::dot(-lDir, lightNormal)) /
                   static_cast<float>(directCheck.t * directCheck.t);
          // add the light to our output
          outColor +=
              absorbtion * area * g *
              evalMaterial(material, lDir, omegaO, normal) *
              scene.materials[scene.triangleMats[directCheck.triangleId / 3]]
                  .eCol;
        }
      }
    }
    // Indirect.
    const auto num = 1;
    for (auto i = 0u; i < num; ++i) {
      // contains phi theta worldDir pdfValue color
      auto samp = generateSampleForMaterial(material, omegaO, normal, ior);
      /* auto newAbsorb = (1.0f / num) * absorbtion * cosf(std::get<1>(sample))
         *
                       evalMaterial(material, worldSample, omegaO, normal) /
                       cosWeightedPDF(std::get<0>(sample), std::get<1>(sample));
                       */
      auto newAbsorb = (1.0f / num) * absorbtion * cosf(std::get<1>(samp)) *
                       std::get<4>(samp) / std::get<3>(samp);
      stack.push(std::make_tuple(newAbsorb, hitPos + ep * std::get<2>(samp),
                                 std::get<2>(samp), currentDepth + 1));
    }
  }
  return outColor;
}
#endif  // BRDF
