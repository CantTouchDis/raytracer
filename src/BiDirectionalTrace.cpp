// Copyright 2017 Bausch Philipp
// E-Mail <bauschphilipp@gmail.com>

#include <BRDF.h>
#include <BVH.h>
#include <BiDirectionalTrace.h>
#include <RayCast.h>
#include <Scene.h>

#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <random>
#include <vector>

namespace {
std::random_device rd;
std::mt19937 generator(rd());
std::uniform_real_distribution<float> bayDist(0, 1);
const float epsilon = 1e-6;
const uint32_t noMatId = static_cast<uint32_t>(-1);

struct NodeValue {
  glm::vec3 comulativeA;
  float comulativeP;
  float gTerm;
  float pTerm;
};

struct PathNode {
  glm::vec3 position;
  glm::vec3 normal;
  uint32_t materialID;
  bool onLight;
};

struct PNode {
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec3 alpha;
  uint32_t materialId;
  float g;
  float prob;
  uint8_t flags;
};
const uint8_t LIGHT = 0x1;
const uint8_t SPECULAR = 0x2;

inline glm::vec3 genPos(const Scene& scene, const uint64_t& firstVertexIndex,
                        float a, float b) {
  return static_cast<float>(1 - a - b) *
             scene.vertices[scene.indices[firstVertexIndex]] +
         a * scene.vertices[scene.indices[firstVertexIndex + 1]] +
         b * scene.vertices[scene.indices[firstVertexIndex + 2]];
}

inline glm::vec3 genNormal(const Scene& scene, const uint64_t& firstVertexIndex,
                           float a, float b) {
  return static_cast<float>(1 - a - b) *
             scene.normals[scene.normalIndices[firstVertexIndex]] +
         a * scene.normals[scene.normalIndices[firstVertexIndex + 1]] +
         b * scene.normals[scene.normalIndices[firstVertexIndex + 2]];
}

inline bool isLight(const BRDFData& mat) { return glm::length2(mat.eCol) > 0; }
inline float calcG(const glm::vec3& n1, const glm::vec3& n2,
                   const glm::vec3& connection, float length2) {
  return fabs(glm::dot(n1, connection) * glm::dot(n2, -connection)) / length2;
}
}
namespace bdpt {
using std::isinf;
using std::isnan;

uint32_t genSPath(const Scene& scene, const glm::vec3& origin,
                  const glm::vec3& direction, bool lightPath,
                  uint16_t maxPathLength, std::vector<PNode>& path) {
  int32_t currentlyTraversedMaterial = -1;
  BRDFData::sampleInfo nextSample;
  glm::vec3 currentVertexPos;
  // The first vertex has to be generated differently for eye- and light-paths.
  if (lightPath) {
    // The path that needs to be generated has to start on a light.
    auto randomArea = bayDist(generator) * scene.totalLightArea;
    auto it = std::lower_bound(scene.comulativeLightArea.begin(),
                               scene.comulativeLightArea.end(), randomArea);
    if (it == scene.comulativeLightArea.end()) return 0u;
    auto firstVertex = scene.importantThings[std::distance(
        scene.comulativeLightArea.begin(), it)];
    auto e1 = scene.vertices[scene.indices[firstVertex + 1]];
    auto e2 = scene.vertices[scene.indices[firstVertex + 2]];
    auto p = scene.vertices[scene.indices[firstVertex]];
    e1 -= p;
    e2 -= p;
    float r1 = 1;
    float r2 = 1;
    // generate a sample on this triangle.
    while (r1 + r2 > 1) {
      r1 = bayDist(generator);
      r2 = bayDist(generator);
    }
    auto lPos = p + r1 * e1 + r2 * e2;
    auto lNormal = genNormal(scene, firstVertex, r1, r2);
    const auto matId = scene.triangleMats[firstVertex / 3];
    const auto& mat = scene.materials[matId];
    int32_t nextTraversedMaterial = matId;
    nextSample = mat.generateSampleFunc(mat, -lNormal, lNormal,
                                        currentlyTraversedMaterial,
                                        nextTraversedMaterial);
    std::get<1>(nextSample) = glm::vec3(1.0f) / static_cast<float>(M_PI);
    currentlyTraversedMaterial = nextTraversedMaterial;
    currentVertexPos = lPos;
    // path[0]
    path.push_back({lPos, lNormal, static_cast<float>(M_PI) * mat.eCol * scene.totalLightArea, matId, 1.0f,
                    1.0f / scene.totalLightArea, LIGHT});
    // TODO(bauschp, this value might be wrong)
  } else {
    path.push_back({origin, direction, /*W_e0 / prob*/ glm::vec3(1.0f), noMatId,
                    1.0f, 1.0f, 0u});
    nextSample = std::make_tuple(direction, /*W_e1*/ glm::vec3(1.0f), 1.0f);
    currentVertexPos = origin;
  }
  // follow the samples and generate the path.
  for (auto i = 0; i < maxPathLength; ++i) {
    auto nextDirection = std::get<0>(nextSample);
    auto brdfValue = std::get<1>(nextSample);  // fs(x_(i-1)->x_(i)->x_(i+1))?
    auto projSolidProb =
        std::get<2>(nextSample);  // P_sigmaOrth(x_(i)->x_(i+1))
    // check if we intersec something.
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    bool hit =
        traverseBVH(currentVertexPos + epsilon * nextDirection, nextDirection,
                    scene.vertices, scene.indices, scene.nodes, 0, &data);
    // if we hit something add the node. otherwise break
    if (!hit) return i + 1;
    auto d = data.t;
    auto oldNormal = path.back().normal;                        // N(x_(i))
    currentVertexPos += static_cast<float>(d) * nextDirection;  // x_(i+1)
    auto normal =
        genNormal(scene, data.vertexIndex, data.a, data.b);  // N(x_(i+1))
    auto matId = scene.triangleMats[data.vertexIndex / 3];
    const auto& mat = scene.materials[matId];

    const auto& oldValue = path.back();
    auto g = calcG(oldNormal, normal, nextDirection, d * d);  // G(xi-1 <-> xi)
    int32_t nextTraversedMaterial = matId;
    nextSample = mat.generateSampleFunc(mat, -nextDirection, normal,
                                        currentlyTraversedMaterial,
                                        nextTraversedMaterial);
    currentlyTraversedMaterial = nextTraversedMaterial;

    // offset a tiny bit in direction of the next vertex.
    nextDirection = std::get<0>(nextSample);
    currentVertexPos += epsilon * nextDirection;
    // Compute the flags for the given material.
    uint8_t flags =
        (isLight(mat) ? LIGHT : 0u) | (mat.evalBRDF == nullptr ? SPECULAR : 0u);
    // add node to path.
    path.push_back({currentVertexPos, normal,
                    oldValue.alpha * (brdfValue / projSolidProb), matId, g,
                    projSolidProb, flags});
  }
  // if we managed to get here paths of length maxPathLength are available.
  return maxPathLength + 1;
}

uint32_t generateSubPath(const Scene& scene, const glm::vec3& origin,
                         const glm::vec3& direction, bool lightPath,
                         uint16_t maxPathLength, std::vector<NodeValue>& values,
                         std::vector<PathNode>& path) {
  glm::vec3 currentPosition(0.0f);
  BRDFData::sampleInfo materialSample;
  int32_t currentlyTraversedMaterial = -1;
  // values[0]
  values.push_back({glm::vec3(1.0f), 1.0f});
  if (lightPath) {
    // The path that needs to be generated has to start on a light.
    auto randomArea = bayDist(generator) * scene.totalLightArea;
    auto it = std::lower_bound(scene.comulativeLightArea.begin(),
                               scene.comulativeLightArea.end(), randomArea);
    if (it == scene.comulativeLightArea.end()) return 0u;
    auto firstVertex = scene.importantThings[std::distance(
        scene.comulativeLightArea.begin(), it)];
    auto e1 = scene.vertices[scene.indices[firstVertex + 1]];
    auto e2 = scene.vertices[scene.indices[firstVertex + 2]];
    auto p = scene.vertices[scene.indices[firstVertex]];
    e1 -= p;
    e2 -= p;
    float r1 = 1;
    float r2 = 1;
    // generate a sample on this triangle.
    while (r1 + r2 > 1) {
      r1 = bayDist(generator);
      r2 = bayDist(generator);
    }
    auto lPos = p + r1 * e1 + r2 * e2;
    auto lNormal = genNormal(scene, firstVertex, r1, r2);
    const auto matId = scene.triangleMats[firstVertex / 3];
    const auto& mat = scene.materials[matId];
    // path[0]
    path.push_back({lPos, lNormal, matId, true});
    // values[1]
    values.push_back(
        {mat.eCol * scene.totalLightArea, 1.0f / scene.totalLightArea});
    // generate a sample to follow
    int32_t nextTraversedMaterial = matId;
    materialSample = mat.generateSampleFunc(mat, -lNormal, lNormal,
                                            currentlyTraversedMaterial,
                                            nextTraversedMaterial);
    currentlyTraversedMaterial = nextTraversedMaterial;
    currentPosition = lPos;
  } else {
    // The path starts from the eye.
    path.push_back({origin, direction, noMatId, false});
    // values[1]
    values.push_back({glm::vec3(1.0f), 1.0f});
    // the next position on the path is in the direction we are looking at.
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    bool hit = traverseBVH(origin, direction, scene.vertices, scene.indices,
                           scene.nodes, 0, &data);
    // if we hit something add the node. otherwise break
    if (!hit) return 1;
    // TODO(bauschp): <o + t * d> might be better.
    currentPosition = genPos(scene, data.vertexIndex, data.a, data.b);
    auto normal = genNormal(scene, data.vertexIndex, data.a, data.b);
    auto matId = scene.triangleMats[data.vertexIndex / 3];
    const auto& mat = scene.materials[matId];
    path.push_back({currentPosition, normal, matId, isLight(mat)});
    int32_t nextTraversedMaterial = matId;
    materialSample = mat.generateSampleFunc(mat, -direction, normal,
                                            currentlyTraversedMaterial,
                                            nextTraversedMaterial);
    currentlyTraversedMaterial = nextTraversedMaterial;
    // values[2]
    values.push_back({glm::vec3(1.0f),
                      static_cast<float>(fabs(glm::dot(-direction, normal)) /
                                         (data.t * data.t))});
  }
  // generate a node for the current pathlength.
  for (uint32_t pLength = path.size(); pLength <= maxPathLength; ++pLength) {
    // check if the next sample exists.
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    auto sampleDir = std::get<0>(materialSample);
    bool hit =
        traverseBVH(currentPosition + epsilon * sampleDir, sampleDir,
                    scene.vertices, scene.indices, scene.nodes, 0, &data);
    // if we hit something add the node. otherwise break
    if (!hit) return pLength;
    auto oldNormal = path.back().normal;
    currentPosition = genPos(scene, data.vertexIndex, data.a, data.b);
    auto normal = genNormal(scene, data.vertexIndex, data.a, data.b);
    auto matId = scene.triangleMats[data.vertexIndex / 3];
    const auto& mat = scene.materials[matId];
    // nodes[pLength]
    path.push_back({currentPosition, normal, matId, isLight(mat)});
    // values[pLength + 1]
    const auto& oldValue = values.back();
    auto brdfVal = std::get<1>(materialSample);
    auto pdfVal = std::get<2>(materialSample);
    if (pdfVal < 0.0f) {
      std::cout << "THAT SHOULD NOT HAPPEN!";
      exit(0);
    }
    values.push_back(
        {oldValue.comulativeA * brdfVal / pdfVal,
         oldValue.comulativeP * pdfVal *
             static_cast<float>(fabs(glm::dot(oldNormal, sampleDir) *
                                     glm::dot(-sampleDir, normal)) /
                                (data.t * data.t))});
    int32_t nextTraversedMaterial = matId;
    materialSample = mat.generateSampleFunc(mat, -sampleDir, normal,
                                            currentlyTraversedMaterial,
                                            nextTraversedMaterial);
    /*if (mat.evalBRDF == nullptr && lightPath && pLength == 2) {
      std::cout << "Got hit from " << glm::to_string(-sampleDir)
                << " normal: " << glm::to_string(normal) << " will continue
    towards "
                << glm::to_string(std::get<0>(materialSample)) << std::endl;
    }*/
    currentlyTraversedMaterial = nextTraversedMaterial;
  }
  return maxPathLength;
}

float calculateWeight(const std::vector<PNode>& lightPath,
                      const std::vector<PNode>& eyePath, uint16_t s,
                      uint16_t k) {
  return 1.0f / (k + 2);
  auto sum = 0.0f;
  // start at s and apply fraction repeatly p_{i+1}/p_i
  auto pi = 1.0f;
  for (auto i = s; s <= k; ++i) {
    auto j = k - i;
    // if we start at i == 0
    if (i == 0) {
      pi *= lightPath[0].prob / (eyePath[j].prob * eyePath[j].g);
      // skip is specular.
      bool oneIsSpecular = (eyePath[j].flags & SPECULAR) == SPECULAR;
      if (oneIsSpecular) continue;
    } else if (i == k) {
      // TODO(bauschp): Our camera model is not intersectable by rays.
      break;
    } else {
      // Normal case.
      pi *= (lightPath[i].prob * lightPath[i].g) /
            (eyePath[j].prob * eyePath[j].g);
      // skip is specular.
      bool oneIsSpecular = (eyePath[j].flags & SPECULAR) == SPECULAR ||
                           (lightPath[i].flags & SPECULAR) == SPECULAR;
      if (oneIsSpecular) continue;
    }
    sum += pi * pi;
  }
  pi = 1.0f;
  // p_{i}/p_{i+1}
  for (auto i = s; s > 0; ++i) {
    auto j = k + 1 - i;
    if (i == k + 1) {
      // check if we start at the max
      pi *= eyePath[j].prob / (lightPath[i - 1].prob * lightPath[i - 1].g);
      // skip is specular.
      bool oneIsSpecular = (lightPath[i - 1].flags & SPECULAR) == SPECULAR;
      if (oneIsSpecular) continue;
    } else if (i == 1) {
      // TODO(bausch): check for impossible lights
      pi *= (eyePath[j].prob * eyePath[j].g) /
            (lightPath[0].prob);
    } else {
      pi *= (eyePath[j].prob * eyePath[j].g) /
            (lightPath[i - 1].prob * lightPath[i - 1].g);
      // skip is specular.
      bool oneIsSpecular = (eyePath[j].flags & SPECULAR) == SPECULAR ||
                           (lightPath[i - 1].flags & SPECULAR) == SPECULAR;
      if (oneIsSpecular) continue;
    }
    sum += pi * pi;
  }
  return sum == 0.0f ? 0.0f : 1.0f / sum;
}

uint16_t generateSubPath2(const Scene& scene, const glm::vec3& origin,
                          const glm::vec3& direction, bool lightPath,
                          uint16_t maxPathLength,
                          std::vector<NodeValue>& values,
                          std::vector<PathNode>& path) {
  int32_t currentlyTraversedMaterial = -1;
  BRDFData::sampleInfo nextSample;
  glm::vec3 currentVertexPos;
  // values[0]
  values.push_back({glm::vec3(1.0f), 1.0f, 1.0f, 1.0f});
  if (lightPath) {
    // The path that needs to be generated has to start on a light.
    auto randomArea = bayDist(generator) * scene.totalLightArea;
    auto it = std::lower_bound(scene.comulativeLightArea.begin(),
                               scene.comulativeLightArea.end(), randomArea);
    if (it == scene.comulativeLightArea.end()) return 0u;
    auto firstVertex = scene.importantThings[std::distance(
        scene.comulativeLightArea.begin(), it)];
    auto e1 = scene.vertices[scene.indices[firstVertex + 1]];
    auto e2 = scene.vertices[scene.indices[firstVertex + 2]];
    auto p = scene.vertices[scene.indices[firstVertex]];
    e1 -= p;
    e2 -= p;
    float r1 = 1;
    float r2 = 1;
    // generate a sample on this triangle.
    while (r1 + r2 > 1) {
      r1 = bayDist(generator);
      r2 = bayDist(generator);
    }
    auto lPos = p + r1 * e1 + r2 * e2;
    auto lNormal = genNormal(scene, firstVertex, r1, r2);
    const auto matId = scene.triangleMats[firstVertex / 3];
    const auto& mat = scene.materials[matId];
    // path[0]
    path.push_back({lPos, lNormal, matId, true});
    // values[1]
    values.push_back({mat.eCol * scene.totalLightArea,
                      1.0f / scene.totalLightArea, 1.0f,
                      1.0f / scene.totalLightArea});
    int32_t nextTraversedMaterial = matId;
    nextSample = mat.generateSampleFunc(mat, -lNormal, lNormal,
                                        currentlyTraversedMaterial,
                                        nextTraversedMaterial);
    currentlyTraversedMaterial = nextTraversedMaterial;
    currentVertexPos = lPos;
    // TODO(bauschp, this value might be wrong)
    std::get<1>(nextSample) =
        glm::vec3(1.0f) /
        static_cast<float>(M_PI * glm::dot(std::get<0>(nextSample), lNormal));
  } else {
    // Generate vertices starting from the eye.
    // path[0]
    path.push_back({origin, direction, noMatId, false});
    // values[1]
    values.push_back({glm::vec3(1.0f), 1.0f, 1.0f, 1.0f});
    nextSample = std::make_tuple(direction, glm::vec3(1.0f), 1.0f);
    currentVertexPos = origin;
  }
  // our path has two nodes now (k=1). continue as usual.
  for (auto i = 0u; i < maxPathLength; ++i) {
    auto nextDirection = std::get<0>(nextSample);
    auto brdfValue = std::get<1>(nextSample);  // fs(x_(i-3)->x_(i-2)->x_(i-1))?
    auto projSolidProb =
        std::get<2>(nextSample);  // P_sigmaOrth(x_(i-2)->x_(i-1))
    // check if we intersec something.
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    bool hit =
        traverseBVH(currentVertexPos + epsilon * nextDirection, nextDirection,
                    scene.vertices, scene.indices, scene.nodes, 0, &data);
    // if we hit something add the node. otherwise break
    if (!hit) return i + 1;
    auto d = data.t;
    auto oldNormal = path.back().normal;
    // currentVertexPos = genPos(scene, data.vertexIndex, data.a, data.b);
    currentVertexPos += static_cast<float>(d) * nextDirection;
    // currentVertexPos = genPos(scene, data.vertexIndex, data.a, data.b);
    auto normal = genNormal(scene, data.vertexIndex, data.a, data.b);
    auto matId = scene.triangleMats[data.vertexIndex / 3];
    const auto& mat = scene.materials[matId];

    const auto& oldValue = values.back();
    auto g = calcG(oldNormal, normal, nextDirection, d * d);
    // add the new values.
    values.push_back({oldValue.comulativeA * brdfValue / projSolidProb,
                      oldValue.comulativeP * projSolidProb * g, g,
                      projSolidProb});
    int32_t nextTraversedMaterial = matId;
    nextSample = mat.generateSampleFunc(mat, -nextDirection, normal,
                                        currentlyTraversedMaterial,
                                        nextTraversedMaterial);
    // offset a tiny bit.
    nextDirection = std::get<0>(nextSample);
    currentVertexPos += epsilon * nextDirection;
    path.push_back({currentVertexPos, normal, matId, isLight(mat)});
    currentlyTraversedMaterial = nextTraversedMaterial;
  }
  return path.size();
}

glm::vec3 generateResultFor(const Scene& scene,
                            const std::vector<PNode>& lightPath,
                            const std::vector<PNode>& eyePath, float weight,
                            writeToLightFunc lBuffer, uint16_t s, uint16_t t) {
  // It is assumed that values for s and t are valid for the given paths.
  // compute the connection term
  glm::vec3 connection(0.0f);
  if (s == 0) {
    const auto& eyeNode = eyePath[t - 1];
    bool isLight = (eyeNode.flags & LIGHT) == LIGHT;
    if (isLight) {
      const auto& mat = scene.materials[eyeNode.materialId];
      // L_e = L_e0 * L_e1
      connection =
          (mat.eCol * static_cast<float>(M_PI)) / static_cast<float>(M_PI);
    }
  } else {
    // t should never be 0 here.
    const auto& eyeVertex = eyePath[t - 1];
    const auto& lightVertex = lightPath[s - 1];

    // Connects lightV --> eyeV
    auto connectionDirection = eyeVertex.position - lightVertex.position;
    auto distance2 = glm::length2(connectionDirection);
    connectionDirection = glm::normalize(connectionDirection);
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    bool canSee = true;
    float d2 = std::numeric_limits<float>::max();
    // If we don't hit anything. There is a clear path.
    if (traverseBVH(lightVertex.position, connectionDirection, scene.vertices,
                    scene.indices, scene.nodes, 0, &data)) {
      // If we hit something we have to check if the eyeVertex is closer.
      d2 = data.t;
      d2 *= d2;
      // if we intersect something while checking for visibility
      canSee = d2 > distance2 || fabs(d2 - distance2) < epsilon;
    }
    if (canSee) {
      // the connecting edge is possible.
      // if one of the surfaces is specular we cant connect them.
      bool oneIsSpecular = (eyeVertex.flags & SPECULAR) == SPECULAR ||
                           (lightVertex.flags & SPECULAR) == SPECULAR;
      if (!oneIsSpecular) {
        // none of the two nodes is specular. connect them
        // be careful with t=1
        const auto& eyeMat = scene.materials[eyeVertex.materialId];
        const auto& lightMat = scene.materials[lightVertex.materialId];
        auto from = s >= 2 ? lightPath[s - 2].position - lightVertex.position
                           : connectionDirection;
        auto fl = lightMat.evalBRDF(lightMat, from, connectionDirection,
                                    lightVertex.normal);
        // if we connect directly to the eye fr = w_e1 (1.0f) in our case. and
        // we have to add it to the light image instead.
        if (t == 1) {
          auto g = calcG(lightVertex.normal, -connectionDirection,
                         connectionDirection, distance2);
          // TODO(bauschp): we have to filter here.
          if (glm::dot(eyeVertex.normal, -connectionDirection) > 0) {
              //std::cout << "lf = " << glm::to_string(fl) << " g = " << g << " alpha = " << glm::to_string(lightVertex.alpha) << std::endl;
            lBuffer(-connectionDirection, lightVertex.alpha * fl * g * weight);
          }
        } else {
          auto from = t >= 2 ? eyePath[t - 2].position - eyeVertex.position
                             : -connectionDirection;
          auto fr = eyeMat.evalBRDF(eyeMat, from, -connectionDirection,
                                    lightVertex.normal);

          auto g = calcG(lightVertex.normal, eyeVertex.normal,
                         connectionDirection, distance2);
          connection = fl * g * fr;
        }
      }
    }
  }
  return (s > 0 ? lightPath[s - 1].alpha : glm::vec3(1.0f)) * connection *
         eyePath[t - 1].alpha;
  /*
  if (s == 0) {
    // s == 0 only makes sense if the last node on the eye path was a light.
    const auto& mat = scene.materials[eyePath[t - 1].materialId];
    if (isLight(mat)) {
      connection = mat.eCol;
    }
  } else {
    const auto& eyeVertex = eyePath[t - 1];

    const auto& lightVertex = lightPath[s - 1];

    // Connects lightV --> eyeV
    auto connectionDirection = eyeVertex.position - lightVertex.position;
    auto distance2 = glm::length2(connectionDirection);
    connectionDirection = glm::normalize(connectionDirection);
    IntersectionData data;
    data.t = std::numeric_limits<double>::max();
    bool canSee = true;
    float d2 = std::numeric_limits<float>::max();
    // If we don't hit anything. There is a clear path.
    if (traverseBVH(lightVertex.position, connectionDirection, scene.vertices,
                    scene.indices, scene.nodes, 0, &data)) {
      // If we hit something we have to check if the eyeVertex is closer.
      d2 = data.t;
      d2 *= d2;
      // if we intersect something while checking for visibility
      canSee = d2 > distance2 || fabs(d2 - distance2) < epsilon;
    }
    if (canSee) {
      //      std::cout << "can see each other" << d2 << std::endl;
      const auto& eyeMat = scene.materials[eyeVertex.materialId];
      const auto& lightMat = scene.materials[lightVertex.materialId];
      auto from = s >= 2 ? lightPath[s - 2].position - lightVertex.position
                         : connectionDirection;
      auto fl = lightMat.evalBRDF != nullptr
                    ? lightMat.evalBRDF(lightMat, from, connectionDirection,
                                        lightVertex.normal)
                    : glm::vec3(0.0f, 0.0f, 0.0f);
      if (s == 1) fl = glm::vec3(1.0f);

      if (t == 1) {
        // t == 1 has to be handled differently cause we had to connect the
        // light to the eye.
        auto g = calcG(lightVertex.normal, eyeVertex.normal,
                       connectionDirection, distance2);
        if (glm::dot(eyeVertex.normal, -connectionDirection) > 0) {
          lBuffer(-connectionDirection,
                  lightValues[s].comulativeA * fl * g * weight);
        }
      } else {
        auto from = t >= 2 ? eyePath[s - 2].position - eyeVertex.position
                           : -connectionDirection;
        auto fr = eyeMat.evalBRDF != nullptr
                      ? eyeMat.evalBRDF(eyeMat, from, -connectionDirection,
                                        lightVertex.normal)
                      : glm::vec3(0.0f);

        auto g = static_cast<float>(
                     fabs(glm::dot(lightVertex.normal, connectionDirection) *
                          glm::dot(-connectionDirection, eyeVertex.normal))) /
                 distance2;
        // std::cout << "ln dir rn:" << glm::to_string(lightVertex.normal) <<
        // glm::to_string(connectionDirection) <<
        // glm::to_string(eyeVertex.normal)
        //          << std::endl;
        auto fLength = glm::length2(fl);
        if (isnan(fLength) || isinf(fLength))
          std::cerr << "The fl is invalid!" << std::endl;
        if (isnan(g) || isinf(g)) std::cerr << "The g is invalid!" << std::endl;
        fLength = glm::length2(fr);
        if (isnan(fLength) || isinf(fLength))
          std::cerr << "The fr is invalid!" << std::endl;
        // return eyeValues[t].comulativeA;
        // return lightValues[s].comulativeA;
        // return glm::vec3(1.0f) * g;
        connection = fl * g * fr;
      }
    }
  }
  // const auto& eyeVertex = eyePath[t - 1];

  //  std::cout << "Light Alpha:" << glm::to_string(lightValues[s].comulativeA)
  //            << "\tConnection:" << glm::to_string(connection)
  //            << "\tEye Alpha:" << glm::to_string(eyeValues[t].comulativeA)
  //            << "\tEyeNormal:" << glm::to_string(eyeVertex.normal)
  //            << std::endl;
  return lightValues[s].comulativeA * connection * eyeValues[t].comulativeA;
  */
}

glm::vec3 bdptScene(const Scene& scene, const glm::vec3& origin,
                    glm::vec3& direction, writeToLightFunc lBuffer, uint16_t oS,
                    uint16_t oT, bool onlyLength) {
  // Calculate a sub-path from the camera.
  uint16_t maxSubpathLength = 5;
  std::vector<PNode> eyePath;
  std::vector<NodeValue> eyeValues;
  uint16_t numEyeVerts =
      genSPath(scene, origin, direction, false, maxSubpathLength, eyePath);
  std::vector<PNode> lightPath;
  std::vector<NodeValue> lightValues;
  uint16_t numLightVerts =
      genSPath(scene, origin, direction, true, maxSubpathLength, lightPath);
/*
  for (auto k = 1u; k < numEyeVerts + numLightVerts - 1; ++k) {
    auto sum = 0.0f;
    for (uint16_t s = 0u; s <= std::min<uint16_t>(k + 1, numLightVerts); ++s) {
      auto t = k - s + 1;
      auto w = calculateWeight(lightPath, eyePath, s, k);
      if (isnan(w) || isinf(w))
        std::cout << "for s,t=" << s << "," << t << " produced " << w
                  << std::endl;
      sum += w;
    }
    std::cout << "for k=" << k << "the weight sum is " << sum << std::endl;
  }
  exit(1);
*/
  if (onlyLength) {
    if (oS > numLightVerts) return glm::vec3(0.0f);
    if (oT > numEyeVerts || oT == 0) return glm::vec3(0.0f, 0.0f, 0.0f);
    return generateResultFor(scene, lightPath, eyePath, 1.0f, lBuffer, oS, oT);
  }
  glm::vec3 out(0.0f);
  for (uint16_t k = 1u; k < numEyeVerts + numLightVerts - 1; ++k) {
    for (uint16_t s = 0u; s <= k + 1; ++s) {
      // s many light Vertices + t many eye vertices give a path of length s +
      // t
      // - 1 = k
      auto t = k - s + 1;
      // if any of these values is out of bounds skip it. Also skip t = 0
      // cause
      // this implementation dosn't allow for these paths to exist. (cam is
      // not
      // intersectable)
      if (s > numLightVerts) break;
      if (t > numEyeVerts || t <= 0) continue;
      auto weight = calculateWeight(lightPath, eyePath, s, k);
      if (isnan(weight) || isinf(weight)) {
        /*std::cerr << "The weight is invalid for k=" << k << " (s, t)=(" << s
                  << ", " << t << ")" << std::endl;
        std::cerr << "Weight=" << weight << std::endl;
        std::cerr << "lP=" << lightValues[s].comulativeP << std::endl;
        std::cerr << "eP=" << eyeValues[t].comulativeP << std::endl;*/
      } else
        out += weight * generateResultFor(scene, lightPath, eyePath, weight,
                                          lBuffer, s, t);
    }
  }
  return out;
}
}
